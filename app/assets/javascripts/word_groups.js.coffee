# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/


@getReadings = (renderCount) ->
  jQuery("#readings_area_#{renderCount}").empty()
  id = jQuery("#word_group_words_writing_#{renderCount}").children("option:selected").val()
  # alert jQuery(this).attr("id").match(/\d+/)
  # id = jQuery(this).children("option:selected").val()
  jQuery("#readings_area_#{renderCount}").append id + "<br />"
  jQuery.getJSON "/words/#{id}.json", (json) ->
    i = 0
    for wa in json.word_associations
      str =
        "<input type=\"checkbox\"
          name=\"word_group[group_members_attributes][#{renderCount*1000+i}][word_association_id]\"
          value=\"#{wa.id}\" checked=\"checked\" /> #{wa.reading.kana_str} <br />"
      jQuery("#readings_area_#{renderCount}").append str
      i++


jQuery ->
  # $('#word_group_words_writing').tokenInput '/words.json'
  #   theme: 'facebook'

  # jQuery("#word_group_words_writing").change () ->
  #   getReadings()


  # alert "ready!"
  jQuery(".gmcheckbox").click ->
    ret = jQuery(this).attr("checked")
    if `ret == "checked"`
      # alert "#{ret} now checked!"
    else
      # alert "#{ret} now NOT checked!"
      id = jQuery(this).attr("id")
      destroy_id = id.replace "word_association_id", "_destroy"
      jQuery("##{destroy_id}").attr("value", 1)



