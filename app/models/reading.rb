class Reading < ActiveRecord::Base
  has_many :word_associations
  has_many :words, through: :word_associations
  has_many :users, through: :word_associations

  attr_accessible :kana_str

  validates :kana_str, uniqueness: true

  def misaka_str
    Word.translate_to_code kana_str
  end

  def word_association(word)
    self.word_associations.detect { |item|
      item.word == word
    }
  end
end
