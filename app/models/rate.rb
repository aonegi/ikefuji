module Rate
  RATE_16 = 16
  RATE_8 = 8

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    @@rate_hash = { 
      RATE_16 => "16k",
      RATE_8 => "8k" }
    
    def rate_view_list
      @@rate_hash.invert.to_a
    end
    
    def rates
      @@rate_hash.keys
    end
    
  end
end
