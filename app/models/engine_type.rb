module EngineType
  ENGINE_MSK = "msk"
  ENGINE_JULIUS = "julius"

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    @@hash = { 
      ENGINE_MSK => "misaka server",
      ENGINE_JULIUS => "julius server" }
    
    def engine_type_view_list
      @@hash.invert.to_a
    end
    
    def engine_types
      @@hash.keys
    end
  end
end
