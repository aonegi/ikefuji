module GrammarStyle
  GRAMMAR_FREE = "free"
  GRAMMAR_FIXED = "fixed"

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    @@hash = { 
      GRAMMAR_FREE => "Free style grammar",
      GRAMMAR_FIXED => "Fixed style grammar" }
    
    def grammar_style_view_list
      @@hash.invert.to_a
    end
    
    def grammar_styles
      @@hash.keys
    end
  end
end
