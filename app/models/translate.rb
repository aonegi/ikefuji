# -*- coding: utf-8 -*-

module Translate
  
  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    TWO_CHAR_TRANSLATE_HASH = { 
      "でぃ" => "d i ",
      "てぃ" => "t i ",
      "すぃ" => "s i ",
      "ずぃ" => "z i ",
      "きゃ" => "ky a ",
      "きゅ" => "ky u ",
      "きょ" => "ky o ",
      "しゃ" => "sh a ",
      "しゅ" => "sh u ",
      "しぇ" => "sh e ",
      "しょ" => "sh o ",
      "ちゃ" => "ch a ",
      "ちゅ" => "ch u ",
      "ちぇ" => "ch e ",
      "ちょ" => "ch o ",
      "にゃ" => "ny a ",
      "にゅ" => "ny u ",
      "にょ" => "ny o ",
      "ひゃ" => "hy a ",
      "ひゅ" => "hy u ",
      "ひょ" => "hy o ",
      "みゃ" => "my a ",
      "みゅ" => "my u ",
      "みょ" => "my o ",
      "りゃ" => "ry a ",
      "りゅ" => "ry u ",
      "りょ" => "ry o ",
      "ぎゃ" => "gy a ",
      "ぎゅ" => "gy u ",
      "ぎょ" => "gy o ",
      "じゃ" => "j a ",
      "ぢゃ" => "j a ",
      "じゅ" => "j u ",
      "じぇ" => "j e ",
      "じょ" => "j o ",
      "びゃ" => "by a ",
      "びゅ" => "by u ",
      "びょ" => "by o ",
      "ぴゃ" => "py a ",
      "ぴゅ" => "py u ",
      "ぴょ" => "py o ",
      "うぃ" => "w i ",
      "うぇ" => "w e ",
      "うぉ" => "w o ",
      "ふぁ" => "f a ",
      "ふぃ" => "f i ",
      "ふぇ" => "f e ",
      "ふぉ" => "f o ",
    }
    
    ONE_CHAR_TRANSLATE_HASH = { 
      "あ" => "a ",
      "い" => "i ",
      "う" => "u ",
      "え" => "e ",
      "お" => "o ",
      "か" => "k a ",
      "き" => "k i ",
      "く" => "k u ",
      "け" => "k e ",
      "こ" => "k o ",
      "さ" => "s a ",
      "し" => "sh i ",
      "す" => "s u ",
      "せ" => "s e ",
      "そ" => "s o ",
      "た" => "t a ",
      "ち" => "ch i ",
      "つ" => "ts u ",
      "て" => "t e ",
      "と" => "t o ",
      "な" => "n a ",
      "に" => "n i ",
      "ぬ" => "n u ",
      "ね" => "n e ",
      "の" => "n o ",
      "は" => "h a ",
      "ひ" => "h i ",
      "ふ" => "f u ",
      "へ" => "h e ",
      "ほ" => "h o ",
      "ま" => "m a ",
      "み" => "m i ",
      "む" => "m u ",
      "め" => "m e ",
      "も" => "m o ",
      "ら" => "r a ",
      "り" => "r i ",
      "る" => "r u ",
      "れ" => "r e ",
      "ろ" => "r o ",
      "が" => "g a ",
      "ぎ" => "g i ",
      "ぐ" => "g u ",
      "げ" => "g e ",
      "ご" => "g o ",
      "ざ" => "z a ",
      "じ" => "j i ",
      "ず" => "z u ",
      "ぜ" => "z e ",
      "ぞ" => "z o ",
      "だ" => "d a ",
      "ぢ" => "j i ",
      "づ" => "z u ",
      "で" => "d e ",
      "ど" => "d o ",
      "ば" => "b a ",
      "び" => "b i ",
      "ぶ" => "b u ",
      "べ" => "b e ",
      "ぼ" => "b o ",
      "ぱ" => "p a ",
      "ぴ" => "p i ",
      "ぷ" => "p u ",
      "ぺ" => "p e ",
      "ぽ" => "p o ",
      "や" => "y a ",
      "ゆ" => "y u ",
      "よ" => "y o ",
      "わ" => "w a ",
      "ん" => "N ",
      "っ" => "q ",
      "ー" => ": ",
      "を" => "o ",
    }
    
    TWO_CHAR_KANA_REGEXP = Regexp.new(TWO_CHAR_TRANSLATE_HASH.keys.join("|"))
    ONE_CHAR_KANA_REGEXP = Regexp.new(ONE_CHAR_TRANSLATE_HASH.keys.join("|"))
    TWO_CHAR_CODE_REGEXP = Regexp.new(TWO_CHAR_TRANSLATE_HASH.values.join("|"))
    ONE_CHAR_CODE_REGEXP = Regexp.new(ONE_CHAR_TRANSLATE_HASH.values.join("|"))

    def two_char_kana_key_hash
      TWO_CHAR_TRANSLATE_HASH
    end
    
    def one_char_kana_key_hash
      ONE_CHAR_TRANSLATE_HASH
    end
    
    def two_char_code_key_hash
      TWO_CHAR_TRANSLATE_HASH.invert
    end
    
    def one_char_code_key_hash
      ONE_CHAR_TRANSLATE_HASH.invert
    end
    
    def translate_to_code(kana_str)
      double_replaced = kana_str.gsub(TWO_CHAR_KANA_REGEXP, two_char_kana_key_hash)
      double_replaced.gsub(ONE_CHAR_KANA_REGEXP, one_char_kana_key_hash)
    end
    
    def translate_to_kana(code_str)
      double_replaced = code_str.gsub(TWO_CHAR_CODE_REGEXP, two_char_code_key_hash)
      double_replaced.gsub(ONE_CHAR_CODE_REGEXP, one_char_code_key_hash)
    end
    
  end
end
