class Term < ActiveRecord::Base
  attr_accessible :position, :word_group_id

  belongs_to :expression
  belongs_to :word_group
end
