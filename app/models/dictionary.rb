class Dictionary < ActiveRecord::Base
  include GrammarStyle
  include Rate

  attr_accessible :name, :grammar_style, :rate, 
  :dictionary_members_attributes

  #### associations ####
  belongs_to :user
  has_many :words
  has_many :dictionary_members
  has_many :word_groups, through: :dictionary_members, order: :position
  has_many :recog_processes
  has_many :expressions
  
  accepts_nested_attributes_for :dictionary_members, allow_destroy: true

  #### validations ####
  validates :name, presence: true
  validates :grammar_style, :inclusion => { 
    :in => [ GRAMMAR_FREE, GRAMMAR_FIXED ] }
  validates :rate, :inclusion => { 
    :in => [ 8, 16 ] }


=begin
  def make_voca_grammar
    path = Rails.root.to_s + "/tmp/" + dir_name
    if !File.exists? path
      Dir::mkdir path
    end
    # p File.exists? path

    ## for Fixed style grammer rules
    ## create a .voca file
    voca_path = path + "/#{dir_name}.voca"
    voca = File.open voca_path, "w+"
    voca.write "% #{dir_name}\n"

    ## for Free style grammer rules
    ## create a .txt file (dictionary file)
    txt_path = path + "/#{dir_name}.txt"
    txt = File.open txt_path, "w+"
    txt.write "</s>    []      silE\n"
    txt.write "<s>     []      silB\n"
    
    words.each do |w|
      w.readings.each do |r|
        voca.write "#{w.writing}       #{r.misaka_str}\n"
        txt.write "#{w.writing}  [#{w.writing}]        #{r.misaka_str}\n"
      end
      # voca.write "#{w.writing}       #{w.misaka_str}\n"
      # txt.write "#{w.writing}  [#{w.writing}]        #{w.misaka_str}\n"
    end

    voca.write "% NOISE\n"
    voca.write "<sp>    sp\n"
    voca.write "% NS_B\n"
    voca.write "<s>     silB\n"
    voca.write "% NS_E\n"
    voca.write "</s>    silE\n"

    ## for Fixed style grammer rules
    ## create a .grammar file
    grammar_path = path + "/#{dir_name}.grammar"
    grammar = File.open grammar_path, "w+"
    grammar.write "S:      NS_B #{dir_name} NS_E\n"
    # grammar.write "S:      NS_B #{dir_name} NOISE #{dir_name} NS_E"
    grammar.write "S:      NS_B NOISE #{dir_name} NOISE NS_E"

    grammar.close
    voca.close
    txt.close
    
    
    # Dir::rmdir path
    # p File.exists? path
  end

  def dir_name
    name + "_" + user.id.to_s
  end
=end
end
