class WordGroup < ActiveRecord::Base
  # belongs_to :dictionary
  attr_accessible :name,
  :group_members_attributes
  # :word_associations_attributes
  # :word_associations_attributes, :words_attributes 
  
  has_many :dictionary_members
  has_many :dictionaries, through: :dictionary_members

  has_many :terms
  has_many :expressions, through: :terms

  has_many :group_members
  has_many :word_associations, through: :group_members
  has_many :words, through: :word_associations, uniq: true
  has_many :readings, through: :word_associations, uniq: true
  
  accepts_nested_attributes_for :group_members, allow_destroy: true
  # accepts_nested_attributes_for :word_associations
  # accepts_nested_attributes_for :words
  
end
