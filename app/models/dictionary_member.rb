class DictionaryMember < ActiveRecord::Base
  attr_accessible :position, :word_group_id
  
  belongs_to :dictionary
  belongs_to :word_group
  
end
