class RecogProcess < ActiveRecord::Base
  include GrammarStyle
  include Rate
  include RecogProcessStatus
  include EngineType
  
  ## Constant
  # STATUS_NOT_INITIALIZED = "not_initialized"
  # STATUS_RUNNING = "running"
  # STATUS_STOPED = "stoped"
  # STATUS_UNREGISTERED = "unregistered"
  
  belongs_to :dictionary
  attr_accessible :fqdn, :port, :server_ip, :engine_type, :grammar_style, :rate, :status

  validates :fqdn, presence: true
  validates :port, presence: true
  # validates :grammar_style, presence: true
  validates :grammar_style, :inclusion => { 
    :in => RecogProcess.grammar_styles }
  validates :rate, :inclusion => { 
    :in => RecogProcess.rates }
  validates :engine_type, :inclusion => { 
    :in => RecogProcess.engine_types }
  validates :status, presence: true
  

  def name
    dictionary.name + "_" + self.id.to_s
  end

  def make_voca_grammar
    path = Rails.root.to_s + "/tmp/" + name
    if !File.exists? path
      Dir::mkdir path
    end

    #### for Free style grammer rules
    ## create a .txt file (dictionary file)
    txt_path = path + "/#{name}.txt"
    txt = File.open txt_path, "w+"
    txt.write "</s>    []      silE\n"
    txt.write "<s>     []      silB\n"
    
    dictionary.words.each do |w|
      w.readings.each do |r|
        txt.write "#{w.writing}  [#{w.writing}]        #{r.misaka_str}\n"
      end
    end

    #### for Fixed style grammer rules
    ## create a .yomi file
    yomi_path = path + "/#{name}.yomi"
    yomi = File.open yomi_path, "w+"
    dictionary.word_groups.each do |wg|
      yomi.write "% #{wg.name}\n"
      wg.words.each do |w|
        w.readings.each do |r|
          yomi.write "#{w.writing} #{r.kana_str}\n"
        end
      end
    end
    yomi.write "% NOISE\n"
    yomi.write "<sp>    sp\n"
    yomi.write "% NS_B\n"
    yomi.write "<s>     silB\n"
    yomi.write "% NS_E\n"
    yomi.write "</s>    silE\n"

    ## create a .voca file
    voca_path = path + "/#{name}.voca"
    # voca = File.open voca_path, "w+"
    # dictionary.word_groups.each do |wg|
    #   voca.write "% #{wg.name}\n"
    #   wg.words.each do |w|
    #     w.readings.each do |r|
    #       voca.write "#{w.writing}       #{r.misaka_str}\n"
    #     end
    #   end
    # end

    # voca.write "% NOISE\n"
    # voca.write "<sp>    sp\n"
    # voca.write "% NS_B\n"
    # voca.write "<s>     silB\n"
    # voca.write "% NS_E\n"
    # voca.write "</s>    silE\n"

    ## create a .grammar file
    grammar_path = path + "/#{name}.grammar"
    grammar = File.open grammar_path, "w+"
    dictionary.expressions.each do |exp|
      grammar.write "S:      NS_B #{exp.group_str} NS_E\n"
      if exp.word_groups.length == 1
        grammar.write "S:      NS_B NOISE #{exp.group_str('NOISE')} NOISE NS_E\n"
      else
        grammar.write "S:      NS_B #{exp.group_str('NOISE')} NS_E\n"
      end
    end

    ## close all files
    grammar.close
    # voca.close
    yomi.close
    txt.close

    ## convert string encoding to EUC-JP
    # `nkf -e --overwrite #{voca_path}`
    system "nkf -e --overwrite #{txt_path}"
    system "nkf -e --overwrite #{yomi_path}"
    system "perl #{Rails.root}/lib/yomi2voca.pl #{yomi_path} > #{voca_path}"
  end

  def command_status
    path = Rails.root.to_s + "/lib/local/"
    result = `python #{path}utils/ikefuji.py #{fqdn} status #{name}`
  end

  def command_register
    path = Rails.root.to_s + "/lib/local/"
    rrp = Rails.root.to_s
    
    if grammar_style == GRAMMAR_FREE
      p "register free style"
      system "python #{path}utils/MoveDir.py #{fqdn} `bash #{path}utils/CreateDirFree.sh #{name} #{rrp}/tmp/#{name}/#{name}.txt #{rate}k #{port}`"
      system "python #{path}utils/ikefuji.py #{fqdn} registerfree #{name} testpass #{port}"
    elsif grammar_style == GRAMMAR_FIXED
      p "register fixed style"
      system "python #{path}utils/MoveDir.py #{fqdn} `bash #{path}utils/CreateDir.sh #{name} #{rrp}/tmp/#{name}/#{name}.voca #{rrp}/tmp/#{name}/#{name}.grammar #{rate}k #{port}`"
      system "python #{path}utils/ikefuji.py #{fqdn} register #{name} testpass #{port}"
    else
    end
    ret = `echo $?`
    p ret
  end

  def command_start
    path = Rails.root.to_s + "/lib/local/"
    system "python #{path}utils/ikefuji.py #{fqdn} start #{name}"
    ret = `echo $?`
    p ret
  end

  def command_stop
    path = Rails.root.to_s + "/lib/local/"
    system "python #{path}utils/ikefuji.py #{fqdn} stop #{name}"
    ret = `echo $?`
    p ret
  end

  def command_delete
    path = Rails.root.to_s + "/lib/local/"
    system "python #{path}utils/ikefuji.py #{fqdn} delete #{name}"
    ret = `echo $?`
    p ret
  end
  
  #### Julius commands ####
  def command_status_julius
    path = Rails.root.to_s + "/lib/local/"
    result = `python #{path}utils/ikefuji_julius.py #{fqdn} status #{name}`
  end

  def command_register_julius
    path = Rails.root.to_s + "/lib/local/"
    rrp = Rails.root.to_s
    
    p "register julius style"
    system "python #{path}utils/MoveJuliusDir.py #{fqdn} `bash #{path}utils/CreateJuliusDir.sh #{name} #{rrp}/tmp/#{name}/#{name}.voca #{rrp}/tmp/#{name}/#{name}.grammar #{rate}k #{port}`"
    system "python #{path}utils/ikefuji_julius.py #{fqdn} register #{name} testpass #{port}"
    ret = `echo $?`
    p ret
  end

  def command_start_julius
    path = Rails.root.to_s + "/lib/local/"
    system "python #{path}utils/ikefuji_julius.py #{fqdn} start #{name}"
    ret = `echo $?`
    p ret
  end
  
  def command_stop_julius
    path = Rails.root.to_s + "/lib/local/"
    system "python #{path}utils/ikefuji_julius.py #{fqdn} stop #{name}"
    ret = `echo $?`
    p ret
  end
  
  def command_delete_julius
    path = Rails.root.to_s + "/lib/local/"
    system "python #{path}utils/ikefuji_julius.py #{fqdn} delete #{name}"
    ret = `echo $?`
    p ret
  end

end
