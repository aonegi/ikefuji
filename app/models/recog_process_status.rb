module RecogProcessStatus
  NOT_INITIALIZED = "not_initialized"
  REGISTERED = "registered"
  RUNNING = "running"
  STOP = "stop"
  UNREGISTERED = "unregistered"

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    @@hash = { 
      NOT_INITIALIZED => "",
      REGISTERED => "",
      RUNNING => "",
      STOP => "",
      UNREGISTERED => "",
    }
  end
end
