# -*- coding: utf-8 -*-

class Word < ActiveRecord::Base
  include Translate

  attr_accessible :writing, 
  # :word_associations_attributes,
  :readings_attributes

  belongs_to :dictionary
  has_many :word_associations, dependent: :destroy
  has_many :readings, through: :word_associations
  has_many :users, through: :word_associations

  # accepts_nested_attributes_for :word_associations
  accepts_nested_attributes_for :readings, allow_destroy: true


  before_validation :insert_proper_readings

  def kana_str_list
    readings.map { |r| r.kana_str }
  end

  def word_association(reading)
    word_associations.find { |wa| wa.reading == reading }
  end

  # def insert_proper_readings
  #   # p readings
  #   self.readings.each_with_index do |r, index|
  #     ret = Reading.where(kana_str: r.kana_str)
  #     # p "After index:#{index}, length:#{ret.length}"

  #     if ret.length != 0
  #       self.readings[index] = ret.first
  #     end
  #   end
  #   p readings
  # end

  def add_readings(readings_attributes)
    readings_attributes.each do |key, value|
      # ret = Reading.where(kana_str: value[:kana_str])
      ret = Reading.new({ kana_str: value[:kana_str] })#  if ret == nil
      self.readings << ret
    end
  end

  def insert_proper_readings
    # p readings
    self.readings.each_with_index do |r, index|
      # p r
      ret = Reading.where(kana_str: r.kana_str)
      # p "After index:#{index}, length:#{ret.length}"
      if ret.length != 0
        # self.readings[index] = ret.first
        ret_index = self.word_associations.index { |item| item.reading == ret.first }
        if ret_index.nil?
          self.word_associations.new(word: self, reading: ret.first)
          self.readings.delete r
        end
      end
    end
    # p readings
  end


  # def misaka_str
  #   Word.translate self.reading
  # end
end
