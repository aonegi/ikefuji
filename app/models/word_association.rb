class WordAssociation < ActiveRecord::Base
  belongs_to :word
  belongs_to :reading
  belongs_to :user

  has_many :group_members
  # has_many :word_associations, through: :group_members
  
  attr_accessible :word, :reading
end
