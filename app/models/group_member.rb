class GroupMember < ActiveRecord::Base
  belongs_to :word_group
  belongs_to :word_association
  attr_accessible :word_association_id
end
