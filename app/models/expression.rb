class Expression < ActiveRecord::Base
  attr_accessible :name, :terms_attributes

  belongs_to :dictionary
  has_many :terms
  has_many :word_groups, through: :terms, order: :position

  accepts_nested_attributes_for :terms, allow_destroy: true
  # accepts_nested_attributes_for :word_groups, allow_destroy: true

  def group_str(separator = "")
    str = ""
    word_groups.each_with_index do |wg, index|
      if index == 0 || index == word_groups.length
        ;
      else
        str += " #{separator} "
      end
      str += "#{wg.name}"
    end
    str
  end
end
