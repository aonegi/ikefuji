class ExpressionsController < ApplicationController
  def index
  end

  def show
  end

  def new
    @dictionary = Dictionary.find params[:dictionary_id]
    @expression = Expression.new
    @word_groups = @dictionary.word_groups
  end
  
  def create
    p params
    @dictionary = Dictionary.find params[:dictionary_id]
    @expression = Expression.new params[:expression]
    @expression.dictionary = @dictionary

    respond_to do |format|
      if @expression.save
        format.html { redirect_to dictionary_path(@expression.dictionary),
          notice: "Expression(ID:#{@expression.id} '#{@expression.name}') was successfully created."
        }
      else
        @word_groups = @dictionary.word_groups
        format.html { render action: "new" }
      end
    end
  end

  def edit
    @dictionary = Dictionary.find params[:dictionary_id]
    @expression = Expression.find params[:id]
    @word_groups = @dictionary.word_groups
  end

  def update
    @expression = Expression.find params[:id]
    
    respond_to do |format|
      if @expression.update_attributes(params[:expression])
        format.html { redirect_to dictionary_path(@expression.dictionary),
          notice: "Expression(ID:#{@expression.id} '#{@expression.name}') was successfully created."
        }
      else
        @word_groups = @dictionary.word_groups
        format.html { render action: "edit" }
      end
    end
  end
  
  def destory
  end
  
end
