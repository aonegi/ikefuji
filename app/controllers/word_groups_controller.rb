class WordGroupsController < ApplicationController
  # GET /word_groups
  # GET /word_groups.json
  def index
    @word_groups = WordGroup.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @word_groups }
    end
  end

  # GET /word_groups/1
  # GET /word_groups/1.json
  def show
    @word_group = WordGroup.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @word_group }
    end
  end

  # GET /word_groups/new
  # GET /word_groups/new.json
  def new
    @word_group = WordGroup.new
    @word_group.group_members << GroupMember.new
    @words = Word.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @word_group }
    end
  end

  # GET /word_groups/1/edit
  def edit
    @word_group = WordGroup.find(params[:id])
    @words = Word.all
  end

  # POST /word_groups
  # POST /word_groups.json
  def create
    # p params
    params[:word_group].delete(:words)
    if !params[:word].blank?
      if !params[:word_id].blank?
        @word = Word.find(params[:word_id])
        @word.update_attributes(params[:word])
      else
        @word = Word.new(params[:word])
        @word.save
      end
      
      params[:word][:readings_attributes].each_with_index do |ary, index|
        new_wa = WordAssociation.joins(:reading).
          where([ "readings.kana_str = ?", ary[1][:kana_str] ]).first
        new_wa.id
        params[:word_group][:group_members_attributes][((index+1)*10).to_s] = { 
          :word_association_id => new_wa.id.to_s
        }
      end
    end
    
    @word_group = WordGroup.new(params[:word_group])

    respond_to do |format|
      if @word_group.save
        format.html { redirect_to @word_group, notice: 'Word group was successfully created.' }
        format.json { render json: @word_group, status: :created, location: @word_group }
      else
        format.html { render action: "new" }
        format.json { render json: @word_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /word_groups/1
  # PUT /word_groups/1.json
  def update
    params[:word_group].delete(:kana_str)
    params[:word_group].delete(:writing)
    params[:word_group].delete(:words)
    # p params
    @word_group = WordGroup.find(params[:id])

    respond_to do |format|
      if @word_group.update_attributes(params[:word_group])
        format.html { redirect_to @word_group, notice: 'Word group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @word_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /word_groups/1
  # DELETE /word_groups/1.json
  def destroy
    @word_group = WordGroup.find(params[:id])
    @word_group.destroy

    respond_to do |format|
      format.html { redirect_to word_groups_url }
      format.json { head :no_content }
    end
  end
end
