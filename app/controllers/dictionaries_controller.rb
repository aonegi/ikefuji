# -*- coding: utf-8 -*-

require "net/http"
require "uri"
require "csv"
require 'xml/libxml'

class DictionariesController < ApplicationController
  # GET /dictionaries
  # GET /dictionaries.json
  def index
    @dictionaries = Dictionary.order :name

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @dictionaries }
    end
  end

  # GET /dictionaries/1
  # GET /dictionaries/1.json
  def show
    @dictionary = Dictionary.find(params[:id])
    @words = @dictionary.words
    @server = request.host_with_port

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @dictionary }
    end
  end

  # GET /dictionaries/new
  # GET /dictionaries/new.json
  def new
    @dictionary = Dictionary.new
    @grammar_styles = Dictionary.grammar_style_view_list
    @rates = Dictionary.rate_view_list
    @word_groups = WordGroup.all

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @dictionary }
    end
  end

  # GET /dictionaries/1/edit
  def edit
    @dictionary = Dictionary.find(params[:id])
    @grammar_styles = Dictionary.grammar_style_view_list
    @rates = Dictionary.rate_view_list
    @word_groups = WordGroup.all
  end

  # POST /dictionaries
  # POST /dictionaries.json
  def create
    @dictionary = Dictionary.new(params[:dictionary])

    respond_to do |format|
      if @dictionary.save
        format.html { redirect_to @dictionary, notice: 'Dictionary was successfully created.' }
        format.json { render json: @dictionary, status: :created, location: @dictionary }
      else
        format.html { render action: "new" }
        format.json { render json: @dictionary.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /dictionaries/1
  # PUT /dictionaries/1.json
  def update
    @dictionary = Dictionary.find(params[:id])

    respond_to do |format|
      if @dictionary.update_attributes(params[:dictionary])
        format.html { redirect_to @dictionary, notice: 'Dictionary was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @dictionary.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dictionaries/1
  # DELETE /dictionaries/1.json
  def destroy
    @dictionary = Dictionary.find(params[:id])
    @dictionary.destroy

    respond_to do |format|
      format.html { redirect_to dictionaries_url }
      format.json { head :no_content }
    end
  end
  
  # GET /dictionaries/1/evaluate
  def evaluate

    @dictionary = Dictionary.find(params[:id])
    @recog_process = @dictionary.recog_processes.find_by_id params[:recog_process_id]
    match_count = 0
    miss_count = 0
    total_count = 0
    
    csv_path = "#{Rails.root}/sounds/robots/16k/robots_16k.csv"
    reader = CSV.open(csv_path, "r")
    reader.each do |row_ary|
      file_path = "#{Rails.root}/sounds/robots/16k/#{row_ary[0]}"
      msk_xml = post_to_msk file_path, @recog_process, @dictionary
      elements = msk_xml.find "//ELEMENT"
      result = ""
      elements.each do |elem|
        if elem.attributes['PHONE'] == "silB"
          # p "start"
        elsif elem.attributes['PHONE'] == "silE"
          # p "end"
        else
          # p elem.attributes['WORD'] + " " + elem.attributes['PHONE']
          result += elem.attributes['WORD']
        end
      end
      row_ary[1].gsub!(/\s/, "")
      
      total_count += 1
      if result == row_ary[1]
        match_count += 1
        p "match!"
      else
        miss_count += 1
        p "not match!! |#{result}| |#{row_ary[1]}|"
      end
    end
    p "total: #{total_count}, match: #{match_count}, miss: #{miss_count}"
    p "averate: #{match_count / total_count}"
  end

  # GET /dictionaries/1/auto_evaluate
  def auto_evaluate
    @dictionary = Dictionary.find(params[:id])
    @recog_process = @dictionary.recog_processes.find_by_id params[:recog_process_id]
    match_count = 0
    miss_count = 0
    total_count = 0

    @dictionary.expressions.each do |exp|
      p exp.group_str
      word_stack = []
      in_str = ""
      w_ary_stack = Array.new
      test2 exp.word_groups, 0, Array.new, w_ary_stack
      w_ary_stack.each do |w_ary|
        in_str = ""
        w_ary.each { |w| in_str += w.readings.first.kana_str }
        # p in_str
        

        in_file_path = "#{Rails.root}/lib/open_jtalk/in.txt"
        File.open(in_file_path, "w") do |f|
          f.write in_str
        end
        dic   = '/usr/local/share/open_jtalk/open_jtalk_dic_utf_8-1.05'
        f_voice = '/usr/local/share/hts_voice/mei_normal'
        outfile48 = "#{Rails.root}/lib/open_jtalk/out48.wav"
        outfile16 = "#{Rails.root}/lib/open_jtalk/out16.wav"
        outfile8 = "#{Rails.root}/lib/open_jtalk/out8.wav"
        voice = f_voice
        s = [
             "-x  #{dic}",
             "-td #{voice}/tree-dur.inf",
             "-tm #{voice}/tree-mgc.inf",
             "-tf #{voice}/tree-lf0.inf",
             "-tl #{voice}/tree-lpf.inf",
             "-md #{voice}/dur.pdf",
             "-mm #{voice}/mgc.pdf",
             "-mf #{voice}/lf0.pdf",
             "-ml #{voice}/lpf.pdf",
             "-dm #{voice}/mgc.win1",
             "-dm #{voice}/mgc.win2",
             "-dm #{voice}/mgc.win3",
             "-df #{voice}/lf0.win1",
             "-df #{voice}/lf0.win2",
             "-df #{voice}/lf0.win3",
             "-dl #{voice}/lpf.win1",
             "-ow #{outfile48}",
             "-s 48000", # sampling frequency
             "-p 240",
             "-a 0.55", # 高低？
             "-u 0.5", # 有声化・無声化？
             "-em #{voice}/tree-gv-mgc.inf",
             "-ef #{voice}/tree-gv-lf0.inf",
             "-cm #{voice}/gv-mgc.pdf",
             "-cf #{voice}/gv-lf0.pdf",
             "-jm 1.0", # 大小？
             "-jf 1.0", # 抑揚？
             "-k  #{voice}/gv-switch.inf",
             "-z 6000"
            ]
        r = s.join(" ")
        system "open_jtalk #{r} #{in_file_path}"
        system "sox --norm #{outfile48} -r 16000 #{outfile16}"
        system "sox --norm #{outfile48} -r 8000 #{outfile8}"

        msk_xml = post_to_msk outfile16, @recog_process, @dictionary
        elements = msk_xml.find "//ELEMENT"
        result = ""
        elements.each do |elem|
          if elem.attributes['PHONE'] == "silB"
            # p "start"
          elsif elem.attributes['PHONE'] == "silE"
            # p "end"
          else
            # p elem.attributes['WORD'] + " " + elem.attributes['PHONE']
            result += elem.attributes['WORD']
          end
        end
        expected = ""
        w_ary.each { |w| expected += w.writing }
        
        total_count += 1
        if result == expected
          match_count += 1
          p "match!"
        else
          miss_count += 1
          p "not match!! expected: |#{expected}|, but result is: |#{result}| "
        end
        File.delete in_file_path
        File.delete outfile48
        File.delete outfile16
        File.delete outfile8
      end

    end

    p "total: #{total_count}, match: #{match_count}, miss: #{miss_count}"
    p "averate: #{match_count.to_f / total_count}"
  end
  
  def julius_evaluate
    @dictionary = Dictionary.find(params[:id])
    @recog_process = @dictionary.recog_processes.find_by_id params[:recog_process_id]
    
    csv_path = "#{Rails.root}/sounds/robots/16k/robots_16k.csv"
    reader = CSV.open(csv_path, "r")
    reader.each do |row_ary|
      file_path = "#{Rails.root}/sounds/robots/16k/#{row_ary[0]}"
      ret_xml = post_to_julius file_path, @recog_process, @dictionary
    end
  end

private
  
  def post_to_msk(file_path, rp, dictionary)
    uri = URI.parse("http://#{rp.fqdn}/mskserver/gateway.php?username=#{dictionary.name}_#{rp.id}&password=testpass&mode=xml")
    # uri = URI.parse("http://192.168.1.156/mskserver/gateway.php?username=robots_1051294554&password=testpass&mode=xml")
    Net::HTTP.start(uri.host, uri.port) do |http|
      ## create request instance

      request = Net::HTTP::Post.new(uri.request_uri)

      ## header part
      request["user-agent"] = "Ruby/#{RUBY_VERSION} MyHttpClient"
      request.set_content_type("multipart/form-data; boundary=myboundary")
      ## following expresion is also OK
      # request["content-type"] = "multipart/form-data; boundary=myboundary"
      
      ## body part
      ## create body part with the specification of multipart/form-data
      body = ""

      # body.concat("--myboundary\r\n")
      # body.concat("content-disposition: form-data; name=\"id\";\r\n")
      # body.concat("\r\n")
      # body.concat("1\r\n")

      body.concat("--myboundary\r\n")
      body.concat("content-disposition: form-data; name=\"upfile\"; filename=\"recdata_eva5_activate.wav\"\r\n")
      body.concat("\r\n")
      File::open(file_path) { |f| body.concat(f.read + "\r\n") }

      body.concat("--myboundary--\r\n")
      request.body = body
      
      ## send
      response = http.request(request)

      # p response.body
      inxml_str = response.body
      inxml_str.gsub! "<s>", "replaced_start"
      inxml_str.gsub! "</s>", "replaced_end"
      parser = XML::Parser.string(inxml_str,
                                  :encoding => XML::Encoding::UTF_8,
                                  :options => XML::Parser::Options::NOBLANKS)
      parser.parse
    end
  end

  def get_word_stack(org_wg_ary, current_wg, stack_element_org, stack)
    if org_wg_ary.last == current_wg
      current_wg.readings.each do |r|
        clone = stack_element_org.dup
        clone << r.kana_str
        stack << clone
      end
      

    end
  end

  def test2(org_wg_ary, depth, w_ary, w_ary_stack)
    # p "depth #{depth}"
    current_wg = org_wg_ary[depth]
    current_wg.words.each do |w|
      if org_wg_ary.last == current_wg
        # p "last wg"
        clone = w_ary.dup
        clone << w
        w_ary_stack << clone
      else
        # p "more wg exist"
        w_ary_clone = w_ary.dup
        w_ary_clone << w
        # depth = depth + 1
        test2 org_wg_ary, depth + 1, w_ary_clone, w_ary_stack
      end
    end

  end

private
  
  def post_to_msk(file_path, rp, dictionary)
    uri = URI.parse("http://#{rp.fqdn}/mskserver/gateway.php?username=#{dictionary.name}_#{rp.id}&password=testpass&mode=xml")
    # uri = URI.parse("http://192.168.1.156/mskserver/gateway.php?username=robots_1051294554&password=testpass&mode=xml")
    Net::HTTP.start(uri.host, uri.port) do |http|
      ## create request instance

      request = Net::HTTP::Post.new(uri.request_uri)

      ## header part
      request["user-agent"] = "Ruby/#{RUBY_VERSION} MyHttpClient"
      request.set_content_type("multipart/form-data; boundary=myboundary")
      ## following expresion is also OK
      # request["content-type"] = "multipart/form-data; boundary=myboundary"
      
      ## body part
      ## create body part with the specification of multipart/form-data
      body = ""

      # body.concat("--myboundary\r\n")
      # body.concat("content-disposition: form-data; name=\"id\";\r\n")
      # body.concat("\r\n")
      # body.concat("1\r\n")

      body.concat("--myboundary\r\n")
      body.concat("content-disposition: form-data; name=\"upfile\"; filename=\"recdata_eva5_activate.wav\"\r\n")
      body.concat("\r\n")
      File::open(file_path) { |f| body.concat(f.read + "\r\n") }

      body.concat("--myboundary--\r\n")
      request.body = body
      
      ## send
      response = http.request(request)

      # p response.body
      inxml_str = response.body
      inxml_str.gsub! "<s>", "replaced_start"
      inxml_str.gsub! "</s>", "replaced_end"
      parser = XML::Parser.string(inxml_str,
                                  :encoding => XML::Encoding::UTF_8,
                                  :options => XML::Parser::Options::NOBLANKS)
      parser.parse
    end
  end

  def post_to_julius(file_path, rp, dictionary)
  end

  def get_word_stack(org_wg_ary, current_wg, stack_element_org, stack)
    if org_wg_ary.last == current_wg
      current_wg.readings.each do |r|
        clone = stack_element_org.dup
        clone << r.kana_str
        stack << clone
      end
      

    end
  end

  def test2(org_wg_ary, depth, w_ary, w_ary_stack)
    # p "depth #{depth}"
    current_wg = org_wg_ary[depth]
    current_wg.words.each do |w|
      if org_wg_ary.last == current_wg
        # p "last wg"
        clone = w_ary.dup
        clone << w
        w_ary_stack << clone
      else
        # p "more wg exist"
        w_ary_clone = w_ary.dup
        w_ary_clone << w
        # depth = depth + 1
        test2 org_wg_ary, depth + 1, w_ary_clone, w_ary_stack
      end
    end

  end


end
