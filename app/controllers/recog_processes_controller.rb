class RecogProcessesController < ApplicationController
  before_filter :find_dictionaries,
  only: [ :status, :register, :start, :stop, :unregister ]
  
  def index
    @dictionary = Dictionary.find(params[:dictionary_id])
    @recog_processes = @dictionary.recog_processes

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @recog_processes }
    end
  end

  def show
  end

  # GET /recog_process/new
  # GET /recog_process/new.json
  def new
    @dictionary = Dictionary.find(params[:dictionary_id])
    @recog_process = RecogProcess.new
    @value = { grammar_style: @dictionary.grammar_style,
      rate: @dictionary.rate }
    @grammar_styles = RecogProcess.grammar_style_view_list
    @rates = RecogProcess.rate_view_list
    @engine_types = RecogProcess.engine_type_view_list
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @word }
    end
  end

  # POST /recog_process
  # POST /recog_process.json
  def create
    @dictionary = Dictionary.find params[:dictionary_id]
    @recog_process = RecogProcess.new(params[:recog_process])
    @recog_process.dictionary = @dictionary
    @recog_process.status = "not_initialized"
    
    respond_to do |format|
      if @recog_process.save
        format.html { redirect_to @dictionary,
          notice: "RecogProcess was successfully created."
        }
        format.json { render json: @recog_process, status: :created, location: @recog_process }
      else
        @grammar_styles = RecogProcess.grammar_style_view_list
        @rates = RecogProcess.rate_view_list
        @engine_types = RecogProcess.engine_type_view_list
        @value = { grammar_style: @dictionary.grammar_style,
          rate: @dictionary.rate }
        format.html { render action: "new" }
        format.json { render json: @recog_process.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @dictionary = Dictionary.find(params[:dictionary_id])
    @recog_process = RecogProcess.find(params[:id])
    @grammar_styles = RecogProcess.grammar_style_view_list
    @rates = RecogProcess.rate_view_list
    @engine_types = RecogProcess.engine_type_view_list
  end

  def update
    @dictionary = Dictionary.find params[:dictionary_id]
    @recog_process = RecogProcess.find(params[:id])
    
    respond_to do |format|
      if @recog_process.update_attributes(params[:recog_process])
        format.html { redirect_to @dictionary, notice: "RecogProcess(ID:#{@recog_process.id}) was successfully updated." }
        format.json { head :no_content }
      else
        @grammar_styles = RecogProcess.grammar_style_view_list
        @rates = RecogProcess.rate_view_list
        @engine_types = RecogProcess.engine_type_view_list
        format.html { render action: "edit" }
        format.json { render json: @recog_process.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
  end
  
  def status
    path = Rails.root.to_s + "/lib/local/"
    if @rp.engine_type == RecogProcess::ENGINE_MSK
      result = @rp.command_status
    elsif @rp.engine_type == RecogProcess::ENGINE_JULIUS
      result = @rp.command_status_julius
    else
    end
    
    flash[@rp.id] = result

    respond_to do |format|
      format.html { redirect_to @dictionary }
    end
  end
  
  def register
    @rp.make_voca_grammar
    if @rp.engine_type == RecogProcess::ENGINE_MSK
      @rp.command_register
    elsif @rp.engine_type == RecogProcess::ENGINE_JULIUS
      @rp.command_register_julius
    else
    end
    @rp.status = RecogProcess::REGISTERED
    
    respond_to do |format|
      if @rp.save
        format.html { redirect_to @dictionary, notice: 'REGISTERED RecogProcess successfully.' }
      else
        flash[:error] = 'Some error occured. RecogProcess was not registered.'
        format.html { redirect_to @dictionary }
      end
    end
  end
  
  def start
    if @rp.engine_type == RecogProcess::ENGINE_MSK
      @rp.command_start
    elsif @rp.engine_type == RecogProcess::ENGINE_JULIUS
      @rp.command_start_julius
    else
    end
    
    @rp.status = RecogProcess::RUNNING

    respond_to do |format|
      if @rp.save
        format.html { redirect_to @dictionary, notice: 'STARTED RecogProcess successfully.' }
      else
        flash[:error] = 'Some error occured. RecogProcess was not started.'
        format.html { redirect_to @dictionary }
      end
    end
  end

  def stop
    if @rp.engine_type == RecogProcess::ENGINE_MSK
      @rp.command_stop
    elsif @rp.engine_type == RecogProcess::ENGINE_JULIUS
      @rp.command_stop_julius
    else
    end
    
    @rp.status = RecogProcess::STOP

    respond_to do |format|
      if @rp.save
        format.html { redirect_to @dictionary, notice: 'STOPED RecogProcess successfully.' }
      else
        flash[:error] = 'Some error occured. RecogProcess was not stoped.'
        format.html { redirect_to @dictionary }
      end
    end
  end

  def unregister
    if @rp.engine_type == RecogProcess::ENGINE_MSK
      @rp.command_delete
    elsif @rp.engine_type == RecogProcess::ENGINE_JULIUS
      @rp.command_delete_julius
    else
    end
    
    @rp.status = RecogProcess::UNREGISTERED

    respond_to do |format|
      if @rp.save
        format.html { redirect_to @dictionary, notice: 'UNREGISTERED(DELETED) RecogProcess successfully.' }
      else
        flash[:error] = 'Some error occured. RecogProcess was not unregistered.'
        format.html { redirect_to @dictionary }
      end
    end
  end

protected
  def find_dictionaries
    @dictionary = Dictionary.find(params[:dictionary_id])
    @rp = RecogProcess.find params[:id]
  end
end
