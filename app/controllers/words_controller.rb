class WordsController < ApplicationController
  # GET /words
  # GET /words.json
  def index
    @words = Word.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @words }
    end
  end

  # GET /words/1
  # GET /words/1.json
  def show
    # @dictionary = Dictionary.find params[:dictionary_id]
    @word = Word.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: 
        @word.to_json(include: { 
                        word_associations: {
                          only: [ :id ],
                          include: {
                            reading: { only: [ :id, :kana_str ] }
                          }
                        }
                      }) }
    end
  end

  # GET /words/new
  # GET /words/new.json
  def new
    # p params
    @word = Word.new
    @word.readings << Reading.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @word }
    end
  end

  # GET /words/1/edit
  def edit
    # p params
    @word = Word.find(params[:id])
  end

  # POST /words
  # POST /words.json
  def create
    # p params
    @word = Word.find_by_writing params[:word][:writing]
    @word = Word.new({ writing: params[:word][:writing] }) if @word == nil
    @word.add_readings params[:word][:readings_attributes]
    
    respond_to do |format|
      if @word.save
        format.html { redirect_to words_path,
          notice: "Word(ID:#{@word.id} '#{@word.writing}') was successfully created."
        }
        format.json { render json: @word, status: :created, location: @word }
      else
        format.html { render action: "new" }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /words/1
  # PUT /words/1.json
  def update
    # p params
    @word = Word.find(params[:id])

    respond_to do |format|
      if @word.update_attributes(params[:word])
        format.html { redirect_to words_path, 
          notice: "Word(ID:#{@word.id} '#{@word.writing}') was successfully updated."
        }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @word.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /words/1
  # DELETE /words/1.json
  def destroy
    # p params
    @word = Word.find(params[:id])
    @word.destroy

    respond_to do |format|
      format.html { redirect_to words_path, 
        notice: "Word(ID:#{@word.id} '#{@word.writing}') was successfully deleted." }
      format.json { head :no_content }
    end
  end
end
