require 'pty'
require 'expect'

if ARGV.length == 0
  p "usage: <<script_name>> <<port_num>>"
  exit
end

$expect_verbose = true
command = "adintool -in file -out adinnet -server 192.168.0.6 -port #{ARGV[0]}"


=begin
module Expect
  def spawn(cmd)
    puts "CMD: #{cmd}" if $expect_verbose
    PTY.spawn(cmd) do |r,w,pid|
      @input_stream = r
      @output_stream = w
      @child_pid = pid
      PTY.protect_signal do
        yield
      end
    end
  end

  def expect(pat, timeout=10)
    ret = @input_stream.expect(pat, timeout) do |match|
      raise "expect %s timeout " % (pat.kind_of?(Regexp)? pat.source : pat) unless match
      put_cmd(yield(match))
    end
  end

  private
  def put_cmd(cmd)
    @output_stream.puts(cmd)
  end
end




include Expect
spawn(command) do
  expect("enter filename") { |match| "/vagrant/sounds/saitama.wav" }
end
=end



PTY.spawn(command) do |r, w, pid|
  w.sync = true
  ret = r.expect("enter filename->") do |match| 
    w.puts "/vagrant/sounds/robots/16k/ZZZ_16k.wav"
  end
  ret = r.expect("enter filename->") do |match| 
    w.puts "/vagrant/sounds/recdata_17_30.wav"
  end
  p "the following output is the match result"
  p ret
end
