#!/usr/bin/perl
# HTSボイスに mei_normal を使う場合

use strict;
use warnings;

my $voice = '/usr/local/share/hts_voice/mei_normal';
my $dic   = '/usr/local/share/open_jtalk/open_jtalk_dic_utf_8-1.05';

my @opts = (
            -x  => "$dic",
            -td => "$voice/tree-dur.inf",
            -tm => "$voice/tree-mgc.inf",
            -tf => "$voice/tree-lf0.inf",
            -tl => "$voice/tree-lpf.inf",
            -md => "$voice/dur.pdf",
            -mm => "$voice/mgc.pdf",
            -mf => "$voice/lf0.pdf",
            -ml => "$voice/lpf.pdf",
            -dm => "$voice/mgc.win1",
            -dm => "$voice/mgc.win2",
            -dm => "$voice/mgc.win3",
            -df => "$voice/lf0.win1",
            -df => "$voice/lf0.win2",
            -df => "$voice/lf0.win3",
            -dl => "$voice/lpf.win1",
            -ow => 'out.wav',
            -s  => 48000,
            -p  => 240,  # 話速？
            -a  => 0.55, # 声質？
            -u  => 0.0,  # 有声化・無声化？
            -em => "$voice/tree-gv-mgc.inf",
            -ef => "$voice/tree-gv-lf0.inf",
            -cm => "$voice/gv-mgc.pdf",
            -cf => "$voice/gv-lf0.pdf",
            -jm => 0.7,  # 音量？
            -jf => 0.5,  # 抑揚？
            -k  => "$voice/gv-switch.inf",
            -z  => 6000,
           );

exec( 'open_jtalk', @opts, 'in.txt' ) or die "couldn't exec open_jtalk: $!\n";

exit;
