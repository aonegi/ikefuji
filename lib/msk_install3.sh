#! /bin/sh

USER=aonegi
PW=XN5Mbm3d

sudo expect -c "
set timeout 20
spawn adduser $USER
expect \"Enter new UNIX password:\" ; send \"$PW\r\"
expect \"Retype new UNIX password:\" ; send \"$PW\r\"
expect \"Full Name []\" ; send \"\r\"
expect \"[]\" ; send \"\r\"
expect \"[]\" ; send \"\r\"
expect \"[]\" ; send \"\r\"
expect \"[]\" ; send \"\r\"
expect \"Is the information correct\" ; send \"Y\r\"
interact
"
sudo adduser $USER admin

sudo aptitude install -y apache2 php5 php5-cli lighttpd libfcgi0ldbl libcgi-dev sox vorbis-tools
sudo aptitude install -y python-numpy python-scipy
sudo aptitude install -y libfftw3-3 libfftw3-dev libfftw3-doc


#### following commands are executed by aonegi ####
sudo mkdir /misakanetwork
mkdir msk_server_programs
cd msk_server_programs/
expect -c "
set timeout 20
spawn scp -r $USER@49.212.142.147:msk_server/* ./
expect \"Are you sure you want to continue connecting (yes/no)\" ; send \"yes\r\"
expect \"assword\" ; send \"$PW\r\"
interact
"
tar zxvf exports.tar.gz
tar zxvf mskserver_org.tar.gz
mv mskserver_org mskserver
sudo cp -r exports /misakanetwork/
sudo cp -r mskserver /var/www/
sudo chown -R $USER:$USER /misakanetwork/exports/
sudo chown -R $USER:$USER /var/www/mskserver/
sudo chmod -R 777 /var/www/mskserver/process/
sudo chmod -R 777 /var/www/mskserver/log/
sudo cp crossdomain.xml /var/www/
sudo chown $USER:$USER /var/www/crossdomain.xml
tar zxvf work.tar.gz
sudo cp -r work/ /home/$USER/
sudo chown -R $USER:$USER /home/$USER/work/
sudo cp -r pyssp* /usr/local/lib/python2.6/dist-packages/
sudo cp /etc/apache2/sites-available/default ./default.back
sudo cp default /etc/apache2/sites-available/
sudo /etc/init.d/apache2 restart





# su - aonegi -c "sudo mkdir /misakanetwork;
# mkdir msk_server_programs;
# cd msk_server_programs/;
# scp -r aonegi@49.212.142.147:msk_server/* ./;
# tar zxvf exports.tar.gz;
# tar zxvf mskserver_org.tar.gz;
# mv mskserver_org mskserver;
# sudo cp -r exports /misakanetwork/;
# sudo cp -r mskserver /var/www/;
# sudo chown -R aonegi:aonegi /misakanetwork/exports/;
# sudo chown -R aonegi:aonegi /var/www/mskserver/;
# sudo chmod -R 777 /var/www/mskserver/process/;
# sudo chmod -R 777 /var/www/mskserver/log/;
# sudo cp crossdomain.xml /var/www/;
# sudo chown aonegi:aonegi /var/www/crossdomain.xml;
# tar zxvf work.tar.gz;
# cp -r work/ ~/;
# sudo cp -r pyssp* /usr/local/lib/python2.6/dist-packages/;
# sudo cp /etc/apache2/sites-available/default ./default.back;
# sudo cp default /etc/apache2/sites-available/;
# sudo /etc/init.d/apache2 restart;"
