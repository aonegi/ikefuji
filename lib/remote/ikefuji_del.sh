#! /bin/sh

PATH=/sbin:/usr/sbin:/bin:/usr/bin
TARGET_DIR=/misakanetwork/exports/fcgi/$2
PID_FILE=/tmp/lighttpd.$2.pid
ACCESS_FILE=/tmp/lighttpd.$2.access.log
ERROR_FILE=/tmp/lighttpd.$2.error.log
CONFIG_PHP_FILE=/var/www/mskserver/include/config.php
LOG_DIR=/var/www/mskserver/log/$2
PROCESS_DIR=/var/www/mskserver/process/$2

. /lib/lsb/init-functions


if [ $# -ne 2 ]; then
  echo "Usage: $0 delete RECOG_ID" >&2
  exit 1;
fi

if [ "$2" = "" ]; then
  echo "Usage: $0 delete RECOG_ID" >&2
  exit 2;
fi

case "$1" in
  delete)
    log_daemon_msg "Deleting $2"
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      log_end_msg 2;
      echo "The RECOG_ID $2 is not registered."
      exit 2;
    fi
    # if the process is alive, kill it
    if [ -e "$PID_FILE" ]; then
      sudo -u www-data kill `cat $PID_FILE`
    fi
    
    # Exit if the pid file is there
    if [ -e "$PID_FILE" ]; then
      return 3;
    fi

    sudo sed -i /$2.*__AUTO_GENERATED__/d $CONFIG_PHP_FILE  
    sudo -u www-data rm -f $ACCESS_FILE
    sudo -u www-data rm -f $ERROR_FILE
    sudo -u www-data rm -rf $LOG_DIR
    sudo -u www-data rm -rf $PROCESS_DIR
    rm -rf $TARGET_DIR
    #echo $TARGET_DIR
 
    result=$?
    if [ $? != "0" ]; then
      log_end_msg 3
      exit 3
    fi
    log_end_msg 0
    exit 0
    ;;
  *)
    echo "Usage: $0 delete RECOG_ID" >&2
    exit 1
    ;;
esac
