#! /bin/sh

PATH=/sbin:/usr/sbin:/bin:/usr/bin
BASE_DIR=/home/noel/local/grammar-kit-v4.1
TARGET_DIR=$BASE_DIR/grammars/$2
JULIUS_COMMAND=/usr/local/bin/julius
PID_FILE=/tmp/julius.$2.pid

#. /lib/lsb/init-functions


if [ $# -ne 2 ]; then
  echo "Usage: $0 {start|stop|status|restart} RECOG_ID" >&2
  exit 1;
fi

if [ "$2" = "" ]; then
  echo "Usage: $0 {start|stop|status|restart} RECOG_ID" >&2
  exit 2;
fi

do_start()
{
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      return 2;
    fi
    # Exit if there IS the pid file
    if [ -e "$PID_FILE" ]; then
      return 3;
    fi
    
    $JULIUS_COMMAND -C $TARGET_DIR/jconf -C $BASE_DIR/hmm_ptm.jconf -charconv EUC-JP UTF-8 &
    echo $! > $PID_FILE
    if ! [ $? -eq 0 ]; then
      return 4;
    fi
    
    # Exit if there isn't the pid file
    if ! [ -e "$PID_FILE" ]; then
      return 5;
    fi
    
    return 0
}
do_stop()
{
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      return 2;
    fi
    # Exit if there isn't the pid file
    if ! [ -e "$PID_FILE" ]; then
      return 6;
    fi

    kill `cat $PID_FILE`
    rm $PID_FILE
    if ! [ $? -eq 0 ]; then
      return 7;
    fi
    
    # Exit if there IS the pid file
    if [ -e "$PID_FILE" ]; then
      return 8;
    fi
    
    return 0
}
case "$1" in
  start)
    log_daemon_msg "Starting $2"
    do_start
    result=$?
    log_end_msg $result
    exit $result
    ;;
  stop)
    log_daemon_msg "Stopping $2"
    do_stop
    result=$?
    log_end_msg $result
    exit $result
    ;;
  restart)
    log_daemon_msg "Stopping $2"
    do_stop
    log_end_msg $?
    log_daemon_msg "Starting $2"
    do_start
    result=$?
    log_end_msg $result
    exit $result
    ;;
  status)
    if [ -e "$PID_FILE" ]; then
      PID=`cat $PID_FILE`
      echo "$2 is Running... pid is ($PID)"
      exit 0
    else
      echo "$2 is not Running..."
      exit 1
    fi
    
    ;;
  *)
    #echo "Usage: $SCRIPTNAME {start|stop|restart|reload|force-reload}" >&2
    echo "Usage: $0 {start|stop|status|restart} RECOG_ID" >&2
    exit 1
    ;;
esac
