#! /bin/sh

PATH=/sbin:/usr/sbin:/bin:/usr/bin
BASE_DIR=/home/noel/local/grammar-kit-v4.1
TARGET_DIR=$BASE_DIR/grammars/$2
PID_FILE=/tmp/julius.$2.pid

#. /lib/lsb/init-functions


if [ $# -ne 2 ]; then
  echo "Usage: $0 delete RECOG_ID" >&2
  exit 1;
fi

if [ "$2" = "" ]; then
  echo "Usage: $0 delete RECOG_ID" >&2
  exit 2;
fi

case "$1" in
  delete)
    log_daemon_msg "Deleting $2"
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      log_end_msg 2;
      echo "The RECOG_ID $2 is not registered."
      exit 2;
    fi
    # if the process is alive, kill it
    if [ -e "$PID_FILE" ]; then
      kill `cat $PID_FILE`
      rm $PID_FILE
    fi
    
    # Exit if the pid file is there
    if [ -e "$PID_FILE" ]; then
      return 3;
    fi

    rm -rf $TARGET_DIR
 
    result=$?
    if [ $? != "0" ]; then
      log_end_msg 3
      exit 3
    fi
    log_end_msg 0
    exit 0
    ;;
  *)
    echo "Usage: $0 delete RECOG_ID" >&2
    exit 1
    ;;
esac
