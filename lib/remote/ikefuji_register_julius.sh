#! /bin/sh

PATH=/sbin:/usr/sbin:/bin:/usr/bin
TARGET_DIR=/home/noel/local/grammar-kit-v4.1/grammars/$2
PID_FILE=/tmp/lighttpd.$2.pid
#CONFIG_PHP_FILE=./test/config.php
CONFIG_PHP_FILE=/var/www/mskserver/include/config.php
MKDFA_SCRIPT=/usr/local/bin/mkdfa.pl
#. /lib/lsb/init-functions


#check
if [ $# -ne 6 ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
if [ "$1" = "" ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
if [ "$2" = "" ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
if [ "$3" = "" ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
A=`echo -n $4|sed 's/[0-9]//g' `
if [ -n "$A" ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
if [ $4 -lt 10000 ] || [ $4 -gt 60000 ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
if [ "$5" = "" ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
B=`echo -n $6|sed 's/[0-9]//g' `
if [ -n "$B" ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi
if [ $6 -lt 10000 ] || [ $6 -gt 60000 ]; then
  echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
  exit 1;
fi


case "$1" in
  register)
    log_daemon_msg "Registering $2"
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      log_end_msg 2;
      echo "The RECOG_ID $2 is not installed."
      exit 2;
    fi
    # if the process is alive, kill it
    if [ -e "$PID_FILE" ]; then
      sudo -u www-data kill `cat $PID_FILE`
    fi
    
    # Exit if the pid file is there
    if [ -e "$PID_FILE" ]; then
      return 3;
    fi

    cd $TARGET_DIR
    $MKDFA_SCRIPT $2
    
    result=$?
    if [ $? != "0" ]; then
      log_end_msg 3
      exit 3
    fi
    log_end_msg 0
    exit 0
    ;;
  registerfree)
    log_daemon_msg "Registering $2"
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      log_end_msg 2;
      echo "The RECOG_ID $2 is not installed."
      exit 2;
    fi
    # if the process is alive, kill it
    if [ -e "$PID_FILE" ]; then
      sudo -u www-data kill `cat $PID_FILE`
    fi
    
    # Exit if the pid file is there
    if [ -e "$PID_FILE" ]; then
      return 3;
    fi

    LINE="'$2'=>array('uid'=>$4, 'username'=>'$2', 'password'=>'$3', 'server'=>'$5', 'port'=>$6), #__AUTO_GENERATED__"
    sudo sed -i "s/#__LAST_LINE__/                 $LINE\\n#__LAST_LINE__/" $CONFIG_PHP_FILE

    result=$?
    if [ $? != "0" ]; then
      log_end_msg 3
      exit 3
    fi
    log_end_msg 0
    exit 0
    ;;
  *)
    echo "Usage: $0 {register|registerfree} RECOG_ID password uid[10000-60000] port[10000-60000]" >&2
    exit 1
    ;;
esac
