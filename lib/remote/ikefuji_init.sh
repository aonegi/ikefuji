#! /bin/sh

PATH=/sbin:/usr/sbin:/bin:/usr/bin
TARGET_DIR=/misakanetwork/exports/fcgi/$2
PID_FILE=/tmp/lighttpd.$2.pid

. /lib/lsb/init-functions


if [ $# -ne 2 ]; then
  echo "Usage: $0 {start|stop|status|restart} RECOG_ID" >&2
  exit 1;
fi

if [ "$2" = "" ]; then
  echo "Usage: $0 {start|stop|status|restart} RECOG_ID" >&2
  exit 2;
fi

do_start()
{
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      return 2;
    fi
    # Exit if the pid file is there
    if [ -e "$PID_FILE" ]; then
      return 3;
    fi
    
    sudo -u www-data /usr/sbin/lighttpd -f $TARGET_DIR/lighttpd.conf
    if ! [ $? -eq 0 ]; then
      return 4;
    fi
    
    # Exit if the pid file is not there
    if ! [ -e "$PID_FILE" ]; then
      return 5;
    fi
    
    return 0
}
do_stop()
{
    # Exit if the dir is not installed
    if ! [ -d "$TARGET_DIR" ]; then
      return 2;
    fi
    # Exit if the pid file is not there
    if ! [ -e "$PID_FILE" ]; then
      return 6;
    fi

    sudo -u www-data kill `cat $PID_FILE`
    if ! [ $? -eq 0 ]; then
      return 7;
    fi
    
    # Exit if the pid file is not there
    if [ -e "$PID_FILE" ]; then
      return 8;
    fi
    
    return 0
}
case "$1" in
  start)
    log_daemon_msg "Starting $2"
    do_start
    result=$?
    log_end_msg $result
    exit $result
    ;;
  stop)
    log_daemon_msg "Stopping $2"
    do_stop
    result=$?
    log_end_msg $result
    exit $result
    ;;
  restart)
    log_daemon_msg "Stopping $2"
    do_stop
    log_end_msg $?
    log_daemon_msg "Starting $2"
    do_start
    result=$?
    log_end_msg $result
    exit $result
    ;;
  status)
    if [ -e "$PID_FILE" ]; then
      PID=`cat $PID_FILE`
      echo "$2 is Running... pid is ($PID)"
      exit 0
    else
      echo "$2 is not Running..."
      exit 1
    fi
    
    ;;
  *)
    #echo "Usage: $SCRIPTNAME {start|stop|restart|reload|force-reload}" >&2
    echo "Usage: $0 {start|stop|status|restart} RECOG_ID" >&2
    exit 1
    ;;
esac
