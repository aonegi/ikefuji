module Util

  ## Debug util module for controller
  ## especially useful when functional testing
  module CustomDebug
    def self.included(base)
      @debug = false
      @comp_msg = [ ]
    end
    
    def con_p(*args)
      @comp_msg ||= [ ]
      args.each { |element| @comp_msg <<  element }
      if @debug
        args.each { |element| p element }
      end
    end
  end
end
