#!/bin/bash -e
# $1:RECOG_ID
# $2:voca file
# $3:grammar file
# $4:16k or 8k
# $5:port

PATH=/sbin:/usr/sbin:/bin:/usr/bin
UTILS_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR=`dirname $UTILS_DIR`
WORKING_DIR=$BASE_DIR/working
SKELETON=$BASE_DIR/archive/skeleton.julius

#check
if [ $# -ne 5 ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi

if [ "$1" = "" ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ ! -f $2 ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ ! -f $3 ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ $4 != "16k" ] && [ $4 != "8k" ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
A=`echo -n $5|sed 's/[0-9]//g' `
if [ -n "$A" ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ $5 -lt 10000 ] || [ $5 -gt 60000 ]; then
  echo "Usage: $0 RECOG_ID /path/to/voca /path/to/grammar {16k|8k} port[10000-60000]" >&2
  exit 1;
fi

if [ -e $WORKING_DIR/$1 ]; then
  rm -rf $WORKING_DIR/$1;
fi
#option'r' is not acceptable to MacOSX
cp -Rap $SKELETON $WORKING_DIR/$1

cp $2 $WORKING_DIR/$1/$1.voca
chmod 444 $WORKING_DIR/$1/$1.voca
cp $3 $WORKING_DIR/$1/$1.grammar
chmod 444 $WORKING_DIR/$1/$1.grammar

sed -i s/__ADINNET_PORT__/$5/ $WORKING_DIR/$1/jconf 
mod_port=`expr $5 + 1`
sed -i s/__MODULE_PORT__/$mod_port/ $WORKING_DIR/$1/jconf 

if [ $4 = "8k" ]; then
  sed -i s/#__8k__// $WORKING_DIR/$1/jconf 
fi
if [ $4 = "16k" ]; then
  sed -i s/#__16k__// $WORKING_DIR/$1/jconf
fi

sed -i s/__DICT_NAME__/$1/ $WORKING_DIR/$1/jconf

echo $WORKING_DIR/$1
