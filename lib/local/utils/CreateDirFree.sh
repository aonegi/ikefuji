#!/bin/bash -e
# $1:RECOG_ID
# $2:voca file
# $3:grammar file
# $4:16k or 8k
# $5:port

PATH=/sbin:/usr/sbin:/bin:/usr/bin
UTILS_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR=`dirname $UTILS_DIR`
WORKING_DIR=$BASE_DIR/working
SKELETON=$BASE_DIR/archive/skeleton.free

#check
if [ $# -ne 4 ]; then
  echo "Usage: $0 RECOG_ID /path/to/dict {16k|8k} port[10000-60000]" >&2
  exit 1;
fi

if [ "$1" = "" ]; then
  echo "Usage: $0 RECOG_ID /path/to/dict {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ ! -f $2 ]; then
  echo "Usage: $0 RECOG_ID /path/to/dict {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ $3 != "16k" ] && [ $3 != "8k" ]; then
  echo "Usage: $0 RECOG_ID /path/to/dict {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
A=`echo -n $4|sed 's/[0-9]//g' `
if [ -n "$A" ]; then
  echo "Usage: $0 RECOG_ID /path/to/dict {16k|8k} port[10000-60000]" >&2
  exit 1;
fi
if [ $4 -lt 10000 ] || [ $4 -gt 60000 ]; then
  echo "Usage: $0 RECOG_ID /path/to/dict {16k|8k} port[10000-60000]" >&2
  exit 1;
fi

if [ -e $WORKING_DIR/$1 ]; then
  rm -rf $WORKING_DIR/$1;
fi
#option'r' is not acceptable to MacOSX
cp -Rap $SKELETON $WORKING_DIR/$1

cp $2 $WORKING_DIR/$1/model/lang_m/$1.txt
chmod 444 $WORKING_DIR/$1/model/lang_m/$1.txt

sed -i s/__SERVER_PORT__/$4/ $WORKING_DIR/$1/lighttpd.conf 
sed -i s/__INSTANCE_NAME__/$1/ $WORKING_DIR/$1/lighttpd.conf 

if [ $3 = "8k" ]; then
  sed -i s/#__8k__// $WORKING_DIR/$1/base.conf 
  sed -i s/#__8k__// $WORKING_DIR/$1/p4.conf 
fi
if [ $3 = "16k" ]; then
  sed -i s/#__16k__// $WORKING_DIR/$1/base.conf 
  sed -i s/#__16k__// $WORKING_DIR/$1/p4.conf 
fi

sed -i s/__DICT_NAME__/$1/ $WORKING_DIR/$1/p4.conf

echo $WORKING_DIR/$1
