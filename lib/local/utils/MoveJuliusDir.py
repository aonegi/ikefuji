# -*- coding: utf-8 -*-
import sys
from sys import exit,stdout, stderr
from pexpect import spawn, EOF
import re
import shutil

TARGET_DIR = "/home/noel/local/grammar-kit-v4.1/grammars/"

argvs = sys.argv
argc = len(argvs)

if (argc < 2):
    print 'Usage: $ python %s host dir'% argvs[0]
    quit()

host = argvs[1]
id = 'noel'
pw = 'forest1219'

c = spawn('scp -r %s %s@%s:%s' % (argvs[2], id, host, TARGET_DIR))
#c.logfile = stderr
i = c.expect_exact(['yes', 'assword: ', EOF])
if i == 0: # unknown ssh key
    c.sendline('yes')
    c.expect_exact('assword: ')
    c.sendline(pw)
    c.expect_exact(EOF)
elif i == 1:
	c.sendline(pw)
	c.expect_exact(EOF)
	
c.close()

shutil.rmtree(argvs[2])

