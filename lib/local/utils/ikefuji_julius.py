# -*- coding: utf-8 -*-
import sys
from sys import exit,stdout, stderr
from pexpect import spawn, EOF
import re

SCRIPTS_DIR = "/home/noel/work/remote/"
INIT_SCRIPT = SCRIPTS_DIR+"ikefuji_init_julius.sh"
REGISTER_SCRIPT = SCRIPTS_DIR+"ikefuji_register_julius.sh"
DEL_SCRIPT = SCRIPTS_DIR+"ikefuji_del_julius.sh"

argvs = sys.argv
argc = len(argvs)

if (argc < 4):
    print 'Usage: $ python %s host {start|stop|status|delete|register|registerfree} REGIST_ID [Options]'% argvs[0]
    quit()

host = argvs[1]
#host = 'm15003.fairydevices.jp'
id = 'noel'
pw = 'forest1219'

def do_start(host, id, pw, recog_id):
    # start process normally : return 0
    # couldn't start process : return error code
    c = spawn('ssh %s@%s' % (id, host))
    i = c.expect_exact(['yes', 'assword: ', '$ '])
    if i == 0: # unknown ssh key
        c.sendline('yes')
        c.expect_exact('assword: ')
        c.sendline(pw)
    if i == 1:
        c.sendline(pw)
    else:
		c.sendline('')
    
    c.expect_exact('$ ')
    c.sendline("bash "+INIT_SCRIPT+" start "+recog_id)
    i = c.expect_exact(['noel:','$ '])
    if i == 0:
      c.sendline(pw)
      c.expect_exact('$ ')
    
    c.sendline('echo $?')
    c.readline() # skip remote echo
    i = int(c.readline().strip("\r\n"))

    c.expect_exact('$ ')
    c.sendline('exit') 

    c.expect_exact(EOF)
    c.close()
    
    return i

def do_stop(host, id, pw, recog_id):
    # stop process normally : return 0
    # couldn't stop process : return error code
    c = spawn('ssh %s@%s' % (id, host))
    i = c.expect_exact(['yes', 'assword: ', '$ '])
    if i == 0: # unknown ssh key
        c.sendline('yes')
        c.expect_exact('assword: ')
        c.sendline(pw)
    if i == 1:
        c.sendline(pw)
    else:
		c.sendline('')
    
    c.expect_exact('$ ')
    c.sendline("bash "+INIT_SCRIPT+" stop "+recog_id)
    i = c.expect_exact(['noel:','$ '])
    if i == 0:
      c.sendline(pw)
      c.expect_exact('$ ')
    
    c.sendline('echo $?')
    c.readline() # skip remote echo
    i = int(c.readline().strip("\r\n"))

    c.expect_exact('$ ')
    c.sendline('exit') 

    c.expect_exact(EOF)
    c.close()
    
    return i

def get_status(host, id, pw, recog_id):
    # if process is running : return pid
    # if process is stopping : return 0
    c = spawn('ssh %s@%s' % (id, host))
    i = c.expect_exact(['yes', 'assword: ', '$ '])
    if i == 0: # unknown ssh key
        c.sendline('yes')
        c.expect_exact('assword: ')
        c.sendline(pw)
    if i == 1:
        c.sendline(pw)
    else:
		c.sendline('')
    
    c.expect_exact('$ ')
    c.sendline("bash "+INIT_SCRIPT+" status "+recog_id)
    c.readline() # skip remote echo
    pid_line = c.readline()
    
    c.expect_exact('$ ')
    c.sendline('echo $?')
    c.readline() # skip remote echo
    i = int(c.readline().strip("\r\n"))
    
    c.expect_exact('$ ')
    c.sendline('exit')
    
    c.expect_exact(EOF)
    c.close()

    if i == 0:
        r = re.compile("\([0-9]*\)")
        pid = int(r.search(pid_line).group(0).replace("(","").replace(")",""))
        return pid
    else:
        return 0

def do_register(host, id, pw, recog_id, password, port):
    # register entry normally : return 0
    # couldn't register entry : return error code
    c = spawn('ssh %s@%s' % (id, host))
    i = c.expect_exact(['yes', 'assword: ', '$ '])
    if i == 0: # unknown ssh key
        c.sendline('yes')
        c.expect_exact('assword: ')
        c.sendline(pw)
    if i == 1:
        c.sendline(pw)
    else:
        c.sendline('')
    
    c.expect_exact('$ ')
    #c.sendline("bash "+REGISTER_SCRIPT+" register "+recog_id+" "+password+" "+uid+" "+server+" "+port)
    c.sendline("bash "+REGISTER_SCRIPT+" register "+recog_id+" "+password+" "+port+" 127.0.0.1 "+port)
    i = c.expect_exact(['noel:','$ '])
    if i == 0:
      c.sendline(pw)
      c.expect_exact('$ ')
    
    c.sendline('echo $?')
    c.readline() # skip remote echo
    i = int(c.readline().strip("\r\n"))

    c.expect_exact('$ ')
    c.sendline('exit') 

    c.expect_exact(EOF)
    c.close()
    
    return i

def do_del(host, id, pw, recog_id):
    # delete entry normally : return 0
    # couldn't delete entry : return error code
    c = spawn('ssh %s@%s' % (id, host))
    i = c.expect_exact(['yes', 'assword: ', '$ '])
    if i == 0: # unknown ssh key
        c.sendline('yes')
        c.expect_exact('assword: ')
        c.sendline(pw)
    if i == 1:
        c.sendline(pw)
    else:
		c.sendline('')
    
    c.expect_exact('$ ')
    c.sendline("bash "+DEL_SCRIPT+" delete "+recog_id)
    i = c.expect_exact(['noel:','$ '])
    if i == 0:
      c.sendline(pw)
      c.expect_exact('$ ')
    
    c.sendline('echo $?')
    c.readline() # skip remote echo
    i = int(c.readline().strip("\r\n"))

    c.expect_exact('$ ')
    c.sendline('exit') 

    c.expect_exact(EOF)
    c.close()
    
    return i

if (argvs[2] == "start" and argc == 4):
    # start process normally : return 0
    # couldn't start process : return error code
    result = do_start(host,id,pw,argvs[3])
    if result == 0:
        pid = get_status(host,id,pw,argvs[3])
        stdout.write("Start "+argvs[3]+"@"+host+" successfully. pid is %d\n"%(pid)) 
        exit(0)
    else:
        stdout.write("Couldn't start "+argvs[3]+"@"+host+"...\n")
        stdout.write("result code is %d\n"%(result))
        exit(result)
elif (argvs[2] == "stop" and argc == 4):
    # stop process normally : return 0
    # couldn't stop process : return error code
    result = do_stop(host,id,pw,argvs[3])
    if result == 0:
        stdout.write("Stop "+argvs[3]+"@"+host+" successfully.\n") 
        exit(0)
    else:
        stdout.write("Couldn't stop "+argvs[3]+"@"+host+"...\n")
        stdout.write("result code is %d\n"%(result))
        exit(result)
elif (argvs[2] == "status" and argc == 4):
    # if process is running : return 0
    # if process is stopping : return 1
    result = get_status(host,id,pw,argvs[3])
    if result != 0:
        stdout.write(argvs[3]+"@"+host+" is running. pid is %d\n"%(result)) 
        exit(0)
    else:
        stdout.write(argvs[3]+"@"+host+" is not running.\n")
        exit(1)
elif (argvs[2] == "register"):
    if ( argc != 6 ):
        print 'Usage: $ python %s host register REGIST_ID password port'% argvs[0]
        exit(1)
    # register entry normally : return 0
    # couldn't register entry : return error code
    #result = do_register(host,id,pw,argvs[3],argvs[4],argvs[5],argvs[6],argvs[7])
    result = do_register(host,id,pw,argvs[3],argvs[4],argvs[5])
    if result == 0:
        stdout.write("Register "+argvs[3]+"@"+host+" successfully.\n") 
        exit(0)
    else:
        stdout.write("Couldn't register "+argvs[3]+"@"+host+"...\n")
        exit(result)
elif (argvs[2] == "delete" and argc == 4):
    # delete entry normally : return 0
    # couldn't delete entry : return error code
    result = do_del(host,id,pw,argvs[3])
    if result == 0:
        stdout.write("Delete "+argvs[3]+"@"+host+" successfully.\n") 
        exit(0)
    else:
        stdout.write("Couldn't delete "+argvs[3]+"@"+host+"...\n")
        exit(result)
else:
    print 'Usage: $ python %s host {start|stop|status|delete|register|registerfree} REGIST_ID [Options]'% argvs[0]
    quit(1)

