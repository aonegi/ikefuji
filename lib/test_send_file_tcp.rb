require "socket"

JULIUS_SERVER_ADR = "192.168.1.8"

s = nil
until s
  begin
    s = TCPSocket.open(JULIUS_SERVER_ADR, 5530)
  rescue => e
    p e
    STDERR.puts "could not connect to Julius server(#{JULIUS_SERVER_ADR})... retry just a second later automatically"
    sleep 10
    retry
  end
end
puts "Successfully connected to Julius adin port (#{JULIUS_SERVER_ADR}:#{10300})"

file_path = "/vagrant/sounds/saitama.wav"
f = File::open(file_path)
content = f.read
p content
s.write content

s.close
