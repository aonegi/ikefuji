# -*- coding: utf-8 -*-
require "socket"
# require 'xml/libxml'
require "nokogiri"
require "nkf"


JULIUS_SERVER_ADR = "192.168.0.6"

if ARGV.length == 0
  p "usage: <<script_name>> <<port_num>>"
  exit
end

s = nil
until s
  begin
    s = TCPSocket.open(JULIUS_SERVER_ADR, ARGV[0])
  rescue
    STDERR.puts "could not connect to Julius server(#{JULIUS_SERVER_ADR})... retry just a second later automatically"
    sleep 10
    retry
  end
end
puts "Successfully connected to Julius server(#{JULIUS_SERVER_ADR})"

source = ""
while true
  ret = IO::select([s])
  ret[0].each do |sock|
    source += sock.recv(65535)
    if source[-2..source.size] == ".\n"
      source.gsub!(/\.\n/, "")
=begin
      source.gsub! "<s>", "replaced_start"
      source.gsub! "</s>", "replaced_end"
      begin
        parser = XML::Parser.string(source,
                                    :encoding => XML::Encoding::UTF_8,
                                    :options => (XML::Parser::Options::NOBLANKS | XML::Parser::Options::RECOVER) )
        ret_xml = parser.parse
        p ret_xml
      rescue
        p "failed parsing return xml"
        p source
      end
=end
      p source.encoding
      xml = Nokogiri(source)
      p xml.to_s
      # words = (xml/"RECOGOUT"/"SHYPO"/"WHYPO").inject("") {|ws, w| ws + w["WORD"] }
      # p words
      # words = (xml/"RECOGOUT"/"SHYPO"/"WHYPO").inject("") {|ws, w| ws + w["WORD"] }
      # unless words == ""
      #   twit(words)
      #   puts "「#{words}」を twitしました。"
      # end
      source = ""
    end
  end
end

