package com.orangepanda.model
{
	public class Model
	{
		/**************** Properties ********************/
		private var _observerList:Vector.<IObserver>;
		public function get observerList():Vector.<IObserver>
		{ return this._observerList; }
		
		
		/**************** Constructor ********************/
		public function Model()
		{
			this._observerList = new Vector.<IObserver>();
		}
		
		/**************** public methods *******************/
		public function notify():void
		{
			for each (var o:IObserver in this.observerList) o.update();
		}
		
		public function setObserver(o:IObserver):void
		{
			var index:int = this.observerList.indexOf(o);
			if (index != -1) {
				// this.osmLogManager.info("this observer object is already set to observer list");
			} else {
				this.observerList.push(o);
			}
		}
		
		public function removeObserver(o:IObserver):void
		{
			var index:int = this.observerList.indexOf(o);
			if(index != -1) {
				this.observerList.splice(index, 1);
			} else {
//				this.osmLogManager.warn("Model.removeObserver, " + 
//					"observer passed, but this object is not a observer of this Model.");
			}
		}
		
	}
}