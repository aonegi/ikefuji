package com.orangepanda.model
{
	import spark.components.DropDownList;

	public class RecogProcess extends Model
	{
		/**************** Properties ********************/
		private var _id:int
		public function get id():int
		{ return this._id; }
		
		private var _dictionaryID:int;
		public function get dictionaryID():int
		{ return this._dictionaryID; }
		
		private var _fqdn:String;
		public function get fqdn():String
		{ return this._fqdn; }
		
		private var _port:int;
		public function get port():int
		{ return this._port; }
		
		private var _grammarStyle:String;
		public function get grammarStyle():String
		{ return this._grammarStyle; }
		
		private var _status:String;
		public function get status():String
		{ return this._status; }
		
		public var rpddl:DropDownList;
		
		/**************** Constructor ********************/
		public function RecogProcess()
		{
			super();
		}
		
		/**************** Public Methods ********************/
		internal function parseJson(jsonObj:Object):void
		{
			this._id = jsonObj.id;
			this._dictionaryID = jsonObj.dictionary_id;
			this._fqdn = jsonObj.fqdn;
			this._port = jsonObj.port;
			this._grammarStyle = jsonObj.grammar_style;
			this._status = jsonObj.status;
			// this.rpddl.dataProvider
		}
	}
}