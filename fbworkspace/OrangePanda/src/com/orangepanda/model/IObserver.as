package com.orangepanda.model
{
	public interface IObserver
	{
		function update():void;
	}

}