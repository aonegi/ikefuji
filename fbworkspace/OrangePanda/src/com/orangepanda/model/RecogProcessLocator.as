package com.orangepanda.model
{
	import com.adobe.serialization.json.JSON;
	
	import mx.controls.Alert;
	import mx.managers.CursorManager;
	import mx.messaging.AbstractConsumer;
	import mx.rpc.events.FaultEvent;
	import mx.rpc.events.ResultEvent;
	import mx.rpc.http.HTTPService;

	public class RecogProcessLocator extends Model
	{
		/**************** Static Properties ********************/
		private static var _instance:RecogProcessLocator = new RecogProcessLocator();
		public static function get instace():RecogProcessLocator
		{ return RecogProcessLocator._instance; }
		
		/**************** Properties ********************/
		private var _list:Array;
		public function get list():Array
		{ return this._list; }
		
		private var _https:HTTPService
		public function get https():HTTPService
		{ return this._https; }
		
		private var _serverAddress:String;
		public function get serverAddress():String
		{ return this._serverAddress; }
		
		/**************** Constructor ********************/
		public function RecogProcessLocator()
		{
			this._list = new Array();
			this._https = new HTTPService();
			this.https.addEventListener(ResultEvent.RESULT, this.onHttpServResult);
			this.https.addEventListener(FaultEvent.FAULT, this.onHttpsServFault);
			
			this._serverAddress = "";
		}
		
		/**************** Public Methods ********************/
		public function init(serverAddress:String):void
		{
			this._serverAddress = serverAddress;
		}
		
		public function getRecogProcesses(dicID:String):Boolean
		{
			if (this.serverAddress == "") {
				trace("not initialized");
				return false;
			}
			var url:String = "http://" + this.serverAddress +
				"/dictionaries/"+ dicID +"/recog_processes.json";
			this.https.url = url;
			this.https.send();
			CursorManager.setBusyCursor();
			
			return true;
		}
		
		public function createInstanceFromJson(jsonObj:Object):RecogProcess
		{
			var rp:RecogProcess = new RecogProcess();
			rp.parseJson(jsonObj);
			this.list.push(rp);
			
			return rp;
		}
		
		/**************** Protected Methods ********************/
		protected function onHttpServResult(event:ResultEvent):void
		{
			CursorManager.removeBusyCursor();
			var retJson:Object = JSON.decode(event.result.toString());
			for each (var element:Object in retJson)
			{
				var rp:RecogProcess = this.createInstanceFromJson(element);
				trace(rp.id, rp.fqdn, rp.port, rp.grammarStyle, rp.status);
			}
			this.notify();
		}
		
		protected function onHttpsServFault(event:FaultEvent):void
		{
			CursorManager.removeBusyCursor();
			Alert.show("Http fault event");
		}
	}
}