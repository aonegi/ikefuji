package com.orangepanda.model
{
	public class Constant
	{
		
		public static var SAMPLE_DICTIONARY_LIST:Array = new Array(
			{label:"日付", data:["http://www.chapek.jp/bphone/demo/input_wave_a.php?mode=json",
				"ご予約希望の日付は何月何日ですか？たとえば1月1日のように月日を一緒にお答えください。"]},
			{label:"時刻", data:["http://www.chapek.jp/bphone/demo/input_wave_b.php?mode=json",
				"ご予約希望のお時間は10時から20時のうち何時からをご希望ですか？たとえば13時半からをご希望の場合は13時半や午後1時30分のように30分単位でお答えください。"]},
			{label:"会員登録", data:["http://www.chapek.jp/bphone/demo/input_wave_c.php?mode=json",
				"現在会員登録をされていますか？はい又はいいえでお答えください。"]},
			{label:"会員番号確認", data:["http://www.chapek.jp/bphone/demo/input_wave_d.php?mode=json",
				"ご登録の会員番号は何番ですか？たとえば「ゼロイチニサンシゴ」のように、ゼロも含めた６ケタの数字をお答えください。"]},
			{label:"メニュー確認", data:["http://www.chapek.jp/bphone/demo/input_wave_e.php?mode=json",
				"今回ご予約のメニューはどうされますか？「カット」「パーマ」「カラー」「トリートメント」「マッサージ」のうちいずれかを組み合わせてお答えください。たとえばカットとパーマをご希望の場合は「カットとパーマ」とお答えください。"]},
			{label:"パーマメニュー確認", data:["http://www.chapek.jp/bphone/demo/input_wave_f.php?mode=json",
				"パーマをご希望のお客様は「ストレートパーマ」「縮毛矯正」「デジタルパーマ」「エアウェーブパーマ」のいずれをご希望ですか？"]},
			{label:"カラーメニュー確認", data:["http://www.chapek.jp/bphone/demo/input_wave_g.php?mode=json",
				"カラーをご希望のお客様は「ノーマル」「ブリーチ」「メンテナンス」「リタッチ」のいずれをご希望ですか？"]},
			{label:"マッサージメニュ確認", data:["http://www.chapek.jp/bphone/demo/input_wave_h.php?mode=json",
				"マッサージをご希望のお客様は「ヘッドスパ」「フェイシャルエステ」「ハンドマッサージ」のいずれをご希望ですか？"]},
			{label:"電話形態チェック", data:["http://www.chapek.jp/bphone/demo/input_wave_i.php?mode=json",
				"お客様の電話番号をお伺いいたします。お使いの電話番号が固定電話など１０ケタの場合は「固定電話」、携帯電話など１１ケタの場合は「携帯電話」とお答えください。"]},
			{label:"11ケタ電話番号", data:["http://www.chapek.jp/bphone/demo/input_wave_j.php?mode=json",
				"お客様の電話番号は何番ですか？たとえば「ゼロキュウゼロイチニサンシゴロクシチハチ」の様に間にハイフンを入れずお答えください"]},
			{label:"10kケタ電話番号", data:["http://www.chapek.jp/bphone/demo/input_wave_k.php?mode=json",
				"お客様の電話番号は何番ですか？たとえば「ゼロサンイチニサンシゴロクナナハチ」の様に市外局番から間にハイフンを入れず１０ケタの数字をお答えください"]},
			{label:"確認汎用", data:["http://www.chapek.jp/bphone/demo/input_wave_l.php?mode=json",
				"確認をします○月○日○時から(menu)で予約を受付いたします、よろしいですか？はい又はいいえでお答えください。"]}
		);
		
		public function Constant()
		{
		}
	}
}