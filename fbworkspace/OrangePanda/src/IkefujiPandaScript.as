
import com.adobe.audio.format.WAVWriter;
import com.adobe.serialization.json.JSON;
import com.orangepanda.model.Constant;
import com.orangepanda.model.RecogProcess;
import com.orangepanda.model.RecogProcessLocator;

import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.SampleDataEvent;
import flash.events.SecurityErrorEvent;
import flash.media.Microphone;
import flash.media.Sound;
import flash.media.SoundChannel;
import flash.net.FileReference;
import flash.net.URLLoader;
import flash.net.URLLoaderDataFormat;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;
import flash.system.Security;
import flash.utils.ByteArray;

import mx.collections.ArrayCollection;
import mx.controls.Alert;
import mx.core.FlexGlobals;
import mx.events.FlexEvent;
import mx.managers.CursorManager;

import spark.components.DropDownList;

[Bindable]
private var _dictionaryList:ArrayCollection = new ArrayCollection(
	Constant.SAMPLE_DICTIONARY_LIST
);

[Bindable]
private var _recogProcessList:ArrayCollection;

[Bindable]
private var _samlingRateList:ArrayCollection = new ArrayCollection(
	[{label:"16k", data:"16"},
	{label:"8k", data:"8"}]
);

[Bindable]
private var _noiseReducList:ArrayCollection = new ArrayCollection(
	[ {label:"none", data:"0"},
	{label:"power", data:"1"},
	{label:"mmse", data:"2"},
	{label:"jmap", data:"3"},
	{label:"normal", data:"4"},
	{label:"all", data:"all"} ]
);



/******** Visual components ********/
public var rpddl:DropDownList;




/******** Properties ********/
private var _mic:Microphone;
public function get mic():Microphone
{ return this._mic; }

private var _recData:ByteArray;
public function get recData():ByteArray
{ return this._recData; }

private var _serverAddress:String;
public function get serverAddress():String
{ return this._serverAddress; }

private var _dictionaryID:int;
public function get dictionaryID():int
{ return this._dictionaryID; }

private var _dictionaryName:String;
public function get dictionaryName():String
{ return this._dictionaryName }

public var sc:SoundChannel;


/**** Initializer ****/
protected function ikefujiPanda_applicationCompleteHandler(event:FlexEvent):void
{
	// TODO Auto-generated method stub
	this._recData = new ByteArray();
	
	
	var params:Object = FlexGlobals.topLevelApplication.parameters;
	
	var errStr:String = "";
	if (params.server == null || params.server == "") {
		errStr += "Server address is null.\n";
	} else {
		this._serverAddress = params.server;
		this.logTextArea.appendText(this.serverAddress + "\n");
	}
	
	if (params.dictionaryID == null || params.dictionaryID == "") {
		errStr += "dictionaryID is null.\n";
	} else {
		this._dictionaryID = params.dictionaryID;
		// logTextArea.appendText(this.serverAddress + "\n");
	}
	
	if (params.dictionaryName == null || params.dictionaryName == "") {
		errStr += "dictionaryName is null.\n";
	} else {
		this._dictionaryName = params.dictionaryName;
		// logTextArea.appendText(this.serverAddress + "\n");
	}
	
	if (errStr != "") {
		this.logTextArea.appendText(errStr + "\n");
	}
	
	RecogProcessLocator.instace.observerList.push(this);
	RecogProcessLocator.instace.init(this.serverAddress);
	RecogProcessLocator.instace.getRecogProcesses(this.dictionaryID.toString());
}


/**** Event Handlers ****/
protected function recButton_clickHandler(event:MouseEvent):void
{
	trace("recButton");
	this._mic = Microphone.getMicrophone();
	if (mic != null) {
		// this.mic.addEventListener(ActivityEvent.ACTIVITY, onActive);
		
		mic.setSilenceLevel(0, 5000);
		mic.gain = 50;
		// mic.rate = 44;
		mic.rate = 16;
		
		this.recData.clear();
		this.recData.position = 0;
		this.mic.addEventListener(SampleDataEvent.SAMPLE_DATA, this.onSampleData);
		this.logTextArea.appendText("Rec has started." + "\n");
	}
}

protected function onSampleData(event:SampleDataEvent):void
{
	while (event.data.bytesAvailable)
	{
		// バイトストリームからデータを読み取る
		var sample:Number = event.data.readFloat();
		var volume:Number = Math.abs(sample) * 1000;
		// trace(volume);
		this.volumeBar.setProgress(volume, 200);
		// var sample:Number = event.data.readShort();
		// バイト配列にデータを書き込む
		this.recData.writeFloat(sample);
		// this.recData.writeShort(sample);
	}
}

protected function stopButton_clickHandler(event:MouseEvent):void
{
	this.mic.removeEventListener(SampleDataEvent.SAMPLE_DATA, this.onSampleData);
	this.recData.position = 0;
	this.logTextArea.appendText("Rec has ended." + "\n");
}

protected function playButton_clickHandler(event:MouseEvent):void
{
	this.recData.position = 0;
	var sound:Sound = new Sound();
	sound.addEventListener(SampleDataEvent.SAMPLE_DATA, soundSampleDataHandler);
	sc = sound.play();
	sc.addEventListener(Event.SOUND_COMPLETE, soundCompleteHandler);
	this.logTextArea.appendText("Now playing." + "\n");
	
}

protected function soundSampleDataHandler(event:SampleDataEvent):void
{
	// logTextArea.appendText("sound sample data" + "\n");
	var i :int;
	var bytes :ByteArray = event.data;
	var sample :Number;
	
	// for (i = 0; i < 8192 && recData.bytesAvailable > 0; i++)
	for (i = 0; i < 2048 && recData.bytesAvailable > 0; i++)
	{
		sample = this.recData.readFloat();
		
		for (var j:int = 0; j < 6; j++) bytes.writeFloat(sample);
	}
}

protected function soundCompleteHandler(event:Event):void
{
	sc.removeEventListener(event.type, arguments.callee);
	this.logTextArea.appendText("sound complete" + "\n");
}

protected function playStopButton_clickHandler(event:MouseEvent):void
{
	this.sc.stop();
}

protected function clickBtn_clickHandler(event:MouseEvent):void
{
	var wavWriter:WAVWriter = new WAVWriter();
	
	// バイト配列の読み取り開始位置を最初の0にする
	this.recData.position = 0;
	// WAVWriterで必要なプロパティの設定
	wavWriter.numOfChannels = 1;
	wavWriter.sampleBitRate = 16;
	wavWriter.samplingRate = 16000;
	
	var fr:FileReference = new FileReference();
	var wavBA:ByteArray = new ByteArray();
	wavWriter.processSamples(wavBA, this.recData, 16000, 1);
	fr.save(wavBA, "recdata.wav");
	this.logTextArea.appendText("save complete" + "\n");
}

protected function sendBtn_clickHandler(event:MouseEvent):void
{
	var wavWriter:WAVWriter = new WAVWriter();
	
	// バイト配列の読み取り開始位置を最初の0にする
	this.recData.position = 0;
	// WAVWriterで必要なプロパティの設定
	wavWriter.numOfChannels = 1;
	wavWriter.sampleBitRate = 16;
	wavWriter.samplingRate = 16000;
	
	var wavBA:ByteArray = new ByteArray();
	wavWriter.processSamples(wavBA, this.recData, 16000, 1);
	
	var item:RecogProcess = this._recogProcessList[this.recogProcessDDL.selectedIndex];
	
	Security.loadPolicyFile("http://" + item.fqdn + "/crossdomain.xml");
	var urlRequest : URLRequest = new URLRequest();
	urlRequest.url = "http://" + item.fqdn + "/mskserver/gateway.php?username=" + this.dictionaryName +
		"_" + item.id.toString() +
		"&password=testpass&mode=xml";
	urlRequest.contentType = 'multipart/form-data; boundary=' + UploadPostHelper.getBoundary();
	urlRequest.method = URLRequestMethod.POST;
	
	urlRequest.data = UploadPostHelper.getPostData( 'upfile', wavBA, "upfile");
	urlRequest.requestHeaders.push( new URLRequestHeader( 'Cache-Control', 'no-cache' ) );
	
	// create the image loader & send the image to the server:<br />
	var urlLoader : URLLoader = new URLLoader();
	urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
	urlLoader.addEventListener(Event.COMPLETE, this.loadComplete);
	urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.onSecurityError);
	CursorManager.setBusyCursor();
	urlLoader.load( urlRequest );
}

protected function loadComplete(event:Event):void
{
	CursorManager.removeBusyCursor();
	var ul:URLLoader = event.currentTarget as URLLoader;
	if (ul.data == "") {
		this.logTextArea.appendText("blank result" + "\n");
	} else {
		// trace(ul.data.toString());
		this.logTextArea.appendText(ul.data.toString() + "\n");
	}
}

protected function onSecurityError(event:SecurityErrorEvent):void
{
	CursorManager.removeBusyCursor();
	this.logTextArea.appendText(event.text + "\n");
}