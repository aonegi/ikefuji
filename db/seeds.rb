# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

require 'kconv'


ActiveRecord::Base::transaction() do
  wg_name = "date"
  file_path = "#{Rails.root}/doc/A_date.txt"
  
  date_wg = WordGroup.find_by_name wg_name
  date_wg = WordGroup.create!({ name: wg_name }) if date_wg == nil

  File.open(file_path, "rb") do |f|
    f.each do |line| 
      ret = line.split(/\t/)
      next if ret.length != 3
      
      ret[2].gsub!("\n", "")
      ret[2].gsub!(":", " :")
      ret[2] += " "
      writing = ret[0].toutf8
      kana_str = Word.translate_to_kana(ret[2])
      
      word = Word.find_by_writing writing
      word = Word.new({ writing: writing }) if word == nil
      reading = Reading.new({ kana_str: kana_str })
      word.readings << reading
      word.save!
      date_wg.word_associations << word.word_association(reading)
      date_wg.save!
    end
  end
end

ActiveRecord::Base::transaction() do
  wg_name = "time"
  file_path = "#{Rails.root}/doc/B_time.txt"
  
  date_wg = WordGroup.find_by_name wg_name
  date_wg = WordGroup.create!({ name: wg_name }) if date_wg == nil

  File.open(file_path, "rb") do |f|
    f.each do |line| 
      ret = line.split(/\t/)
      next if ret.length != 3
      
      ret[2].gsub!("\n", "")
      ret[2].gsub!(":", " :")
      ret[2] += " "
      writing = ret[0].toutf8
      kana_str = Word.translate_to_kana(ret[2])
      
      word = Word.find_by_writing writing
      word = Word.new({ writing: writing }) if word == nil
      reading = Reading.new({ kana_str: kana_str })
      word.readings << reading
      word.save!
      date_wg.word_associations << word.word_association(reading)
      date_wg.save!
    end
  end
end

