# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120614083142) do

  create_table "dictionaries", :force => true do |t|
    t.integer  "user_id"
    t.string   "name"
    t.string   "grammar_style"
    t.integer  "rate"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "dictionary_members", :force => true do |t|
    t.integer  "dictionary_id"
    t.integer  "word_group_id"
    t.integer  "position"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "dictionary_members", ["dictionary_id"], :name => "index_dictionary_members_on_dictionary_id"
  add_index "dictionary_members", ["word_group_id"], :name => "index_dictionary_members_on_word_group_id"

  create_table "expressions", :force => true do |t|
    t.integer  "dictionary_id"
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "expressions", ["dictionary_id"], :name => "index_expressions_on_dictionary_id"

  create_table "group_members", :force => true do |t|
    t.integer  "word_group_id"
    t.integer  "word_association_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "group_members", ["word_association_id"], :name => "index_group_members_on_word_association_id"
  add_index "group_members", ["word_group_id"], :name => "index_group_members_on_word_group_id"

  create_table "readings", :force => true do |t|
    t.string   "kana_str"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "recog_processes", :force => true do |t|
    t.integer  "dictionary_id"
    t.string   "fqdn"
    t.string   "server_ip"
    t.string   "engine_type"
    t.integer  "port"
    t.string   "grammar_style"
    t.integer  "rate"
    t.string   "status"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "recog_processes", ["dictionary_id"], :name => "index_recog_processes_on_dictionary_id"

  create_table "terms", :force => true do |t|
    t.integer  "expression_id"
    t.integer  "word_group_id"
    t.integer  "position"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "terms", ["expression_id"], :name => "index_terms_on_expression_id"
  add_index "terms", ["word_group_id"], :name => "index_terms_on_word_group_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "word_associations", :force => true do |t|
    t.integer  "word_id"
    t.integer  "reading_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "word_associations", ["reading_id"], :name => "index_word_associations_on_reading_id"
  add_index "word_associations", ["user_id"], :name => "index_word_associations_on_user_id"
  add_index "word_associations", ["word_id"], :name => "index_word_associations_on_word_id"

  create_table "word_groups", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "words", :force => true do |t|
    t.integer  "dictionary_id"
    t.string   "writing"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "words", ["dictionary_id"], :name => "index_words_on_dictionary_id"

end
