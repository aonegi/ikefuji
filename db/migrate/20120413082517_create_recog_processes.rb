class CreateRecogProcesses < ActiveRecord::Migration
  def change
    create_table :recog_processes do |t|
      t.references :dictionary
      t.string :fqdn
      t.string :server_ip
      t.string :engine_type
      t.integer :port
      t.string :grammar_style
      t.integer :rate
      t.string :status

      t.timestamps
    end
    add_index :recog_processes, :dictionary_id
  end
end
