class CreateReadings < ActiveRecord::Migration
  def change
    create_table :readings do |t|
      t.string :kana_str

      t.timestamps
    end
  end
end
