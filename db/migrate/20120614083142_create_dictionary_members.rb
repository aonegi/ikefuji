class CreateDictionaryMembers < ActiveRecord::Migration
  def change
    create_table :dictionary_members do |t|
      t.references :dictionary
      t.references :word_group
      t.integer :position

      t.timestamps
    end
    add_index :dictionary_members, :dictionary_id
    add_index :dictionary_members, :word_group_id
  end
end
