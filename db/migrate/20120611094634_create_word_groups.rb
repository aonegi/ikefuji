class CreateWordGroups < ActiveRecord::Migration
  def change
    create_table :word_groups do |t|
      # t.references :dictionary
      t.string :name

      t.timestamps
    end
  end
end
