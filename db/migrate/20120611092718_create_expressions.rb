class CreateExpressions < ActiveRecord::Migration
  def change
    create_table :expressions do |t|
      t.references :dictionary
      t.string :name

      t.timestamps
    end
    add_index :expressions, :dictionary_id
  end
end
