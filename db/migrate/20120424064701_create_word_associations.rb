class CreateWordAssociations < ActiveRecord::Migration
  def change
    create_table :word_associations do |t|
      t.references :word
      t.references :reading
      t.references :user

      t.timestamps
    end
    add_index :word_associations, :word_id
    add_index :word_associations, :reading_id
    add_index :word_associations, :user_id
  end
end
