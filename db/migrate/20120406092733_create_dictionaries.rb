class CreateDictionaries < ActiveRecord::Migration
  def change
    create_table :dictionaries do |t|
      t.references :user
      t.string :name
      t.string :grammar_style
      t.integer :rate

      t.timestamps
    end
  end
end
