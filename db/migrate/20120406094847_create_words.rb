class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.references :dictionary
      t.string :writing
      # t.string :reading

      t.timestamps
    end
    add_index :words, :dictionary_id
  end
end
