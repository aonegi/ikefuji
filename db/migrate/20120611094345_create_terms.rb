class CreateTerms < ActiveRecord::Migration
  def change
    create_table :terms do |t|
      t.references :expression
      t.references :word_group
      t.integer :position

      t.timestamps
    end
    add_index :terms, :expression_id
    add_index :terms, :word_group_id
  end
end
