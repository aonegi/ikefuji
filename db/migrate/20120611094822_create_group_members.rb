class CreateGroupMembers < ActiveRecord::Migration
  def change
    create_table :group_members do |t|
      t.references :word_group
      t.references :word_association

      t.timestamps
    end
    add_index :group_members, :word_group_id
    add_index :group_members, :word_association_id
  end
end
