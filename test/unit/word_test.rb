# -*- coding: utf-8 -*-

require 'test_helper'

class WordTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  setup do
    @dictionary = dictionaries :computer

    @writing = "Thinkpad"
    @reading1 = "しんくぱっど"
    @success_param1 = { "writing" => @writing, 
      "readings_attributes" => { "0" => { "kana_str" => @reading1 } }
    }

    @reading2 = "すぃんくぱっど"
    @success_param2 = { "writing" => @writing, 
      "readings_attributes" => { 
        "0" => { "kana_str" => @reading1 },
        "1" => { "kana_str" => @reading2 },
      }
    }

    @reading3 = "しんくぱっと"
    @success_param3 = { "writing" => @writing, 
      "readings_attributes" => { 
        "0" => { "kana_str" => @reading1 },
        "1" => { "kana_str" => @reading2 },
        "2" => { "kana_str" => @reading3 },
      }
    }

    @win = words :win
    @win_reading1 = readings :win_reading1
    @update_param = { 
      "readings_attributes" => { 
        "0" => { "id" => @win_reading1.id.to_s, "kana_str" => "ういんどうずず" },
      }
    }
    @update_param2 = { 
      "readings_attributes" => { 
        "0" => { "kana_str" => "ういんどうずず" },
      }
    }
    @update_param3 = { 
      "readings_attributes" => { 
        "0" => { "kana_str" => "ういんどうずず" },
        "1" => { "kana_str" => "ういんどうずずー" },
        "2" => { "kana_str" => "ういんどうずずあああ" },
      }
    }
  end

  test "translate" do
    assert_equal "m a q k i N t o q sh u ",
    Word.translate_to_code(readings(:mac_reading1).kana_str)
    
    assert_equal "u i N d o u z u ", 
    Word.translate_to_code(readings(:win_reading1).kana_str)
    
    assert_equal "w i N d o u z u ",
    Word.translate_to_code(readings(:win_reading2).kana_str)
    
    assert_equal "k e : k i ",
    Word.translate_to_code("けーき")
    
    assert_equal "k a ch u : sh a ",
    Word.translate_to_code("かちゅーしゃ")
  end

  test "create_with_a_reading" do 
    w = Word.new @success_param1
    assert w.save
    assert_equal @writing, w.writing
    assert_equal 1, w.word_associations.count
    assert_equal 1, w.readings.count
    assert_equal @reading1, w.readings.first.kana_str
    assert_equal "sh i N k u p a q d o ", Word.translate_to_code(w.readings.first.kana_str)
  end

  test "create_with_2_readings" do 
    w = Word.new @success_param2
    assert w.save
    assert_equal @writing, w.writing
    assert_equal 2, w.word_associations.count
    assert_equal 2, w.readings.count
    assert w.kana_str_list.include?(@reading1)
    assert w.kana_str_list.include?(@reading2)
  end
  
  test "create_with_3_readings" do 
    w = Word.new @success_param3
    assert w.save
    assert_equal @writing, w.writing
    assert_equal 3, w.word_associations.count
    assert_equal 3, w.readings.count
    assert w.kana_str_list.include?(@reading1)
    assert w.kana_str_list.include?(@reading2)
    assert w.kana_str_list.include?(@reading3)
    # assert_equal "sh i N k u p a q d o ", Word.translate(w.readings.first.kana_str)
  end

  test "update_existing_reading" do 
    assert_equal 4, @win.readings.count
    assert_equal 4, @win.word_associations.count
    assert @win.kana_str_list.include?(@win_reading1.kana_str)
    
    assert @win.update_attributes(@update_param)
    assert_equal 4, @win.readings.count
    assert_equal 4, @win.word_associations.count
    assert !@win.kana_str_list.include?(@win_reading1.kana_str)
    p @win.kana_str_list
  end

  test "add a new reading to existing word" do 
    assert_equal 4, @win.readings.count
    assert_equal 4, @win.word_associations.count
    
    assert @win.update_attributes(@update_param2)
    
    assert_equal 5, @win.readings.count
    assert_equal 5, @win.word_associations.count

    p @win.kana_str_list
  end

  test "add 3 new readings to existing word" do 
    assert_equal 4, @win.readings.count
    assert_equal 4, @win.word_associations.count
    
    assert @win.update_attributes(@update_param3)
    
    assert_equal 7, @win.readings.count
    assert_equal 7, @win.word_associations.count

    p @win.kana_str_list
  end

  test "delete 1 reading" do
    update_param = { 
      "readings_attributes" => { 
        "0" => { "id" => @win_reading1.id.to_s, "_destroy" => "1" },
      }
    }
    
    assert_equal 4, @win.readings.count
    assert_equal 4, @win.word_associations.count
    
    assert @win.update_attributes(update_param)
    
    assert_equal 3, @win.readings.count
    assert_equal 3, @win.word_associations.count
  end

  test "delete 1 reading and add 2 readings" do
    update_param = { 
      "readings_attributes" => { 
        "0" => { "id" => @win_reading1.id.to_s, "_destroy" => "1" },
        "1" => { "kana_str" => "ういんどうずはいらないこ" },
        "2" => { "kana_str" => "ういんどうずはできるこ" },
      }
    }
    
    assert_equal 4, @win.readings.count
    assert_equal 4, @win.word_associations.count
    
    assert @win.update_attributes(update_param)
    
    assert_equal 5, @win.readings.count
    assert_equal 5, @win.word_associations.count
  end

end
