# -*- coding: utf-8 -*-

require 'test_helper'

class WordGroupsControllerTest < ActionController::TestCase
  setup do
    @word_group = word_groups(:robot_group)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:word_groups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create word_group" do
    prm = { 
      "word" => { 
        "readings_attributes" => { "0" => { "kana_str" => "すとっぷ" } } 
      },
      "word_id" => words(:stop).id.to_s,
      "word_group" => { 
        "name" => "robot_order 4", 
        "group_members_attributes" => {
          "0" => { "word_association_id" => 
            word_associations(:activate_wa1).id.to_s }, 
          "1340185868590" => { "word_association_id" => 
            word_associations(:stop_wa1).id.to_s } } 
      },
    }

    wa_count = WordAssociation.count
    r_count = Reading.count
    gm_count = GroupMember.count
    wg_count = WordGroup.count
    
    assert_difference('WordGroup.count') do
      post :create, prm
    end

    assert_redirected_to word_group_path(assigns(:word_group))
    assert_equal wa_count + 1, WordAssociation.count
    assert_equal r_count + 1, Reading.count
    assert_equal gm_count + 3, GroupMember.count
    assert_equal wg_count + 1, WordGroup.count
  end

  test "should create word_group 2" do
    prm = { 
      "word" => { 
        "writing" => "システム・イド",
        "readings_attributes" => { "0" => { "kana_str" => "しすてむいど" } } 
      },
      "word_group" => { 
        "name" => "robot_order 4", 
        "group_members_attributes" => {
          "0" => { "word_association_id" => 
            word_associations(:activate_wa1).id.to_s }, 
          "1340185868590" => { "word_association_id" => 
            word_associations(:stop_wa1).id.to_s } } 
      },
    }

    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    gm_count = GroupMember.count
    wg_count = WordGroup.count
    
    assert_difference('WordGroup.count') do
      post :create, prm
    end

    assert_redirected_to word_group_path(assigns(:word_group))
    assert_equal word_count + 1, Word.count
    assert_equal wa_count + 1, WordAssociation.count
    assert_equal r_count + 1, Reading.count
    assert_equal gm_count + 3, GroupMember.count
    assert_equal wg_count + 1, WordGroup.count
  end

    test "should create word_group 3" do
    prm = { 
      "word" => { 
        "writing" => "システム・イド",
        "readings_attributes" => { 
          "0" => { "kana_str" => "しすてむいど" },
          "10" => { "kana_str" => "うらこおどざびいすと" },
        } 
      },
      "word_group" => { 
        "name" => "robot_order 4", 
        "group_members_attributes" => {
          "0" => { "word_association_id" => 
            word_associations(:activate_wa1).id.to_s }, 
          "1340185868590" => { "word_association_id" => 
            word_associations(:stop_wa1).id.to_s } } 
      },
    }

    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    gm_count = GroupMember.count
    wg_count = WordGroup.count
    
    assert_difference('WordGroup.count') do
      post :create, prm
    end

    assert_redirected_to word_group_path(assigns(:word_group))
    assert_equal word_count + 1, Word.count
    assert_equal wa_count + 2, WordAssociation.count
    assert_equal r_count + 1, Reading.count
    assert_equal gm_count + 4, GroupMember.count
    assert_equal wg_count + 1, WordGroup.count
  end

  

  test "should show word_group" do
    get :show, id: @word_group
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @word_group
    assert_response :success
  end

  test "should update word_group" do
    ro_group = word_groups(:robot_order)
    prm = { 
      "word_group" => { 
        "name" => "robot_order",
        "group_members_attributes" => {
          "0" => {"_destroy"=>"1", id: group_members(:member_activate).id.to_s },
          "100" => {"_destroy"=>"1", id: group_members(:member_at1).id.to_s },
          "101" => {"_destroy"=>"0",
            "word_association_id"=> word_associations(:stop_wa1).id.to_s,
            id: group_members(:member_stop).id.to_s}, 
          "200" => {"_destroy"=>"0",
            "word_association_id"=>word_associations(:at_wa2).id.to_s,
            id: group_members(:member_at2).id.to_s},
          "300" => {"_destroy"=>"0", 
            "word_association_id"=>word_associations(:dummy_wa1).id.to_s,
            id: group_members(:member_dummy1).id.to_s}, 
          "301" => {"_destroy"=>"0",
            "word_association_id"=>word_associations(:dummy_wa2).id.to_s,
            id: group_members(:member_dummy2).id.to_s}, 
          "400" => {"_destroy"=>"0", 
            "word_association_id"=>word_associations(:beast_wa1).id.to_s,
            id: group_members(:member_beast1).id.to_s}, 
          "401" => { "_destroy"=>"0", 
            "word_association_id"=>word_associations(:beast_wa2).id.to_s,
            id: group_members(:member_beast2).id.to_s} 
          } 
      }, 
      "id" => ro_group.id.to_s
    }
    
    assert_equal 8, ro_group.word_associations.length
    assert_equal 5, ro_group.words.length
    assert_equal 8, ro_group.readings.length

    put :update, prm

    assert_redirected_to word_group_path(assigns(:word_group))
    ro_group.reload
    assert_equal 6, ro_group.word_associations.length
    assert_equal 4, ro_group.words.length
    assert_equal 6, ro_group.readings.length
  end

  test "should destroy word_group" do
    assert_difference('WordGroup.count', -1) do
      delete :destroy, id: @word_group
    end

    assert_redirected_to word_groups_path
  end

end
