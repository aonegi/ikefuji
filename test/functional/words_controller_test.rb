# -*- coding: utf-8 -*-

require 'test_helper'

class WordsControllerTest < ActionController::TestCase
  setup do
    # @dictionary = dictionaries :computer
    @word_group = word_groups :computer_group
    @mac = words :mac

    @success_param = { "word" =>
      { "writing" => "VAIO",
        "readings_attributes" => { "0" => { "kana_str" => "ばいお" } }
      },
    }
    @update_param = { "word" => 
      { "writing" => "Macintosh", 
        "readings_attributes" => { 
          "0" => { "kana_str" => "まっきんとっしゅしゅ", "id" => readings(:mac_reading1).id.to_s },
          "1" => { "kana_str" => "まっく", "id" => readings(:mac_reading2).id.to_s }, 
        }
      },
      "id" => @mac.id.to_s
    }
  end
  
  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:words)
  end

  test "should get new" do
    get :new, { word_group_id: @word_group.id.to_s }
    assert_response :success
  end

  test "should create a word with a reading" do
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    assert_difference('Word.count') do
      post :create, @success_param
    end

    # assert_redirected_to word_group_path(@word_group)
    assert_equal word_count + 1, Word.count
    assert_equal wa_count + 1, WordAssociation.count
    assert_equal r_count + 1, Reading.count
  end

  test "should create a word with some readings" do
    success_param = { "word" =>
      { "writing" => "PC98",
        "readings_attributes" => { 
          "0" => { "kana_str" => "ぴーしーきゅーはち" },
          "1" => { "kana_str" => "ぴいしいきゅうはち" },
          "2" => { "kana_str" => "ぴーしーきゅうはち" },
        }
      },
      "word_group_id" => @word_group.id.to_s
    }
    
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    assert_difference('Word.count') do
      post :create, success_param
    end

    # assert_redirected_to word_group_path(@word_group)
    assert_equal word_count + 1, Word.count
    assert_equal wa_count + 3, WordAssociation.count
    assert_equal r_count + 3, Reading.count
  end

  test "should create a word with a existing reading" do
    # p "START should create a word with a existing reading"
    ff_dic = word_groups(:robot_group)
    success_param = { "word" =>
      { "writing" => "Linux cafe",
        "readings_attributes" => { 
          "0" => { "kana_str" => "りなっくすかふぇ" },
          "1" => { "kana_str" => "りなかふぇ" },
          "2" => { "kana_str" => "りなっくす" },
        }
      },
      "word_group_id" => ff_dic.id.to_s
    }
    
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    assert_difference('Word.count') do
      post :create, success_param
    end

    # assert_redirected_to word_group_path(ff_dic)
    assert_equal word_count + 1, Word.count
    assert_equal wa_count + 3, WordAssociation.count
    assert_equal r_count + 2, Reading.count
    # p "START should create a word with a existing reading"
  end

  test "should get edit" do
    get :edit, { word_group_id: @word_group, id: @mac }
    assert_response :success
    assert_template "edit"
  end

  test "should update word" do
    p "should update word test"
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert !@mac.kana_str_list.include?("まっきんとっしゅしゅ")
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    
    put :update, @update_param

    @mac.reload
    # assert_redirected_to word_group_path(@word_group)
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    assert !@mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert @mac.kana_str_list.include?("まっきんとっしゅしゅ")
    assert_equal word_count, Word.count
    assert_equal wa_count, WordAssociation.count
    assert_equal r_count, Reading.count
    p "END of should update word test"
  end

  test "should update word 2" do
    p "should update word test 2"
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert !@mac.kana_str_list.include?("まっくっく")
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    
    @update_param["word"]["readings_attributes"].delete "0"
    @update_param["word"]["readings_attributes"]["1"] = { 
      "kana_str" => "まっくっく", "id" => readings(:mac_reading2).id.to_s
    }
    
    put :update, @update_param

    @mac.reload
    # assert_redirected_to word_group_path(@word_group)
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert !@mac.kana_str_list.include?("まっく")
    assert @mac.kana_str_list.include?("まっくっく")
    assert_equal word_count, Word.count
    assert_equal wa_count, WordAssociation.count
    assert_equal r_count, Reading.count
    p "END of should update word test 2"
  end

  test "should update a word with deleting some readings" do
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count

    @update_param["word"]["readings_attributes"]["1"] = { "kana_str" => "まっく", "id" => readings(:mac_reading2).id.to_s, "_destroy" => "1" }
    
    put :update, @update_param

    @mac.reload
    # assert_redirected_to word_group_path(@word_group)
    assert_equal 1, @mac.word_associations.length
    assert_equal 1, @mac.readings.length
    assert @mac.kana_str_list.include?("まっきんとっしゅしゅ")
    assert !@mac.kana_str_list.include?("まっく")
    assert_equal word_count, Word.count
    assert_equal wa_count - 1, WordAssociation.count
    assert_equal r_count, Reading.count
  end

  test "should update a word with adding some readings" do
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count

    @update_param["word"]["readings_attributes"]["2"] = { "kana_str" => "あっぷる" }
    
    put :update, @update_param

    @mac.reload
    # assert_redirected_to word_group_path(@word_group)
    assert_equal 3, @mac.word_associations.length
    assert_equal 3, @mac.readings.length
    assert @mac.kana_str_list.include?("まっきんとっしゅしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert @mac.kana_str_list.include?("あっぷる")
    assert_equal word_count, Word.count
    assert_equal wa_count + 1, WordAssociation.count
    assert_equal r_count + 1, Reading.count
  end

  test "should update a word with existing readings" do
    # p "START should update a word with existing readings"
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count

    @update_param["word"]["readings_attributes"]["2"] = { "kana_str" => "りなっくす" }
    
    put :update, @update_param

    @mac.reload
    # assert_redirected_to word_group_path(@word_group)
    assert_equal 3, @mac.word_associations.length
    assert_equal 3, @mac.readings.length
    assert @mac.kana_str_list.include?("まっきんとっしゅしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert @mac.kana_str_list.include?("りなっくす")
    assert_equal word_count, Word.count
    assert_equal wa_count + 1, WordAssociation.count
    assert_equal r_count, Reading.count
    # p "END should update a word with existing readings"
  end

  test "should update a word with existing readings 2" do
    # p "START should update a word with existing readings"
    assert @mac.kana_str_list.include?("まっきんとっしゅ")
    assert @mac.kana_str_list.include?("まっく")
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count

    @update_param["word"]["readings_attributes"]["1"] = 
      { "kana_str" => "りなっくす", "id" => readings(:mac_reading2).id.to_s }
    
    put :update, @update_param

    @mac.reload
    # assert_redirected_to word_group_path(@word_group)
    assert_equal 2, @mac.word_associations.length
    assert_equal 2, @mac.readings.length
    assert @mac.kana_str_list.include?("まっきんとっしゅしゅ")
    assert @mac.kana_str_list.include?("りなっくす")
    assert_equal word_count, Word.count
    assert_equal wa_count, WordAssociation.count
    assert_equal r_count, Reading.count
    mac_ret = Reading.where(kana_str: "まっく")
    assert_equal 1, mac_ret.length
    # p "END should update a word with existing readings"
  end

  test "should destroy word" do
    del_param = { "word_group_id" => @word_group.id.to_s,
      "id" => @mac.id.to_s }
    word_count = Word.count
    wa_count = WordAssociation.count
    r_count = Reading.count
    
    assert_difference('Word.count', -1) do
      delete :destroy, del_param
    end

    # assert_redirected_to dictionary_path(@dictionary)
    assert_equal word_count -1 , Word.count
    assert_equal wa_count - 2, WordAssociation.count
    assert_equal r_count, Reading.count
  end
end
